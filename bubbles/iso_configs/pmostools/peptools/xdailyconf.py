import ttkbootstrap as ttk
import os


# This will set the style to used for bootstrap
# just change the name to what is needed for the
# the system
bbstyle = ttk.Window(themename="peppermint")

# set the title of the window
DEBIAN_TITLE = "xDaily - Automated System Maintenance (Debian)"
DEVUAN_TITLE = "xDaily - Automated System Maintenance (Devuan)"

INTRODUCTION_TEXT =("xDaily offers a seamless and user-friendly system "
                    "updating experience. It seamlessly integrates "
                    "semi-automatic command-line and GUI "
                    "functionalities, ensuring a hassle-free upgrate "
                    "process. With just a simple command, it "
                    "efficiently upgrates your system, encompassing "
                    "essential tasks such as cache cleanup, SSD "
                    "optimization, icon cache updates, and automatic "
                    "removal of outdated repositories. It's a "
                    "comprehensive solution designed to streamline "
                    "and enhance the system maintenance routine"
                    )

CHECK_UPDATES_TEXT =("The 'Check for Updates' feature allows users to "
                     "verify the availability of system updates. By "
                     "simply pressing the 'run' button, xDaily "
                     "promptly checks for any available updates for "
                     "the system, ensuring users stay informed and "
                     "up-to-date with the latest packages"
                     )

VIEW_UPDATES_TEXT =("The 'View Updates' feature offers users a "
                    "convenient way to explore available updates ready "
                    "for installation on the system. By pressing the "
                    "'run' button, xDaily promptly presents a list of "
                    "updates ready for deployment, enabling users to "
                    "stay informed about what updates will be installed"
                    )

NSTALL_UPDATES_TEXT = ("The 'Install Updates' feature empowers users to "
                       "seamlessly install available updates for their "
                       "system. With a simple click of the 'run' "
                       "button, xDaily installs any pending updates, "
                       "ensuring that the system remains up-to-date "
                       "with the latest packages and security patches."
                       )

RUN_APT_CLEAN_TEXT =("The 'Run apt clean' feature provides users with "
                     "a convenient way to optimize disk space usage by "
                     "removing all cached package files. By pressing "
                     "the 'run' button, xDaily clears the cache, "
                     "freeing up valuable disk space. It's important "
                     "to note that this action may require packages to "
                     "be redownloaded later when needed." 
                )

RUN_APT_AUTOCLEAN_TEXT =("The 'Run apt autoclean' feature enables "
                         "users to optimize disk space usage by "
                         "removing only outdated package files from "
                         "the cache, while preserving the cache for "
                         "currently installed packages. By pressing "
                         "the 'run' button, xDaily cleans up "
                         "unnecessary files, freeing up disk space "
                         "without compromising needed package files. "
                         )

RUN_APT_AUTOREMOVE_TEXT = ("The 'Run apt autoremove' feature by "
                           "pressing the 'run' button is generally safe "
                           "and can help free up disk space by "
                           "removing packages that are no longer "
                           "needed. It's a good practice to run apt "
                           "autoremove periodically, especially after "
                           "removing or upgrading packages, to keep "
                           "the system clean and efficient."
                           )

BROWSER_THUMBNAIL_CACHE_TEXT = ("The 'Thumbnail Cache' feature is for "
                                "clearing the browser thumbnail cache "
                                "it can free up disk space, especially "
                                "if the browser cache has grown large "
                                "over time. It can also help resolve "
                                "certain browsing issues, such as "
                                "thumbnails not updating or displaying "
                                "incorrectly."
                                )
CLEAR_RECENTLYUSED_FIREFOX_TEXT = ("The 'Recently Used' option is used "
                                  "to clear the list of recently used "
                                  "files or downloads that Firefox "
                                  "maintains. Downloading files with "
                                  "Firefox, keeps a record of these "
                                  "downloads this list allows for "
                                  "quickly accessing files recently "
                                  "downloaded without having to search "
                                  "for them in the file system."
                                  )
PEPPERMINT_BRANDING_TEXT = ("Sometimes, when updates are applied, the "
                            "upstream branding setting may override "
                            "our custom branding. This feature ensures "
                            "that if any updates remove or alter any "
                            "Perppermint-specific branding, it will be "
                            "set correctly."
                            )
SSD_TRIMMING_TEXT = ("SSD trimming ensures that the operating system "
                     "sends TRIM commands to the SSD, informing it of "
                     "the blocks that are no longer in use. This "
                     "allows the SSD to proactively erase these blocks "
                     "in the background, ensuring that when new data is "
                     "written, it can be stored efficiently without "
                     "the need to first erase old data"
                    )

CACHING_ICONS_TEXT = ("Displaying icons in your desktop environment "
                      "involves reading image files stored on the disk."
                      "This process can slow down system "
                      "responsiveness, particularly with many icons or "
                      "slow disk access speeds."
                      "The caching mechanism helps improve the overall "
                      "responsiveness of the desktop environment. "


)














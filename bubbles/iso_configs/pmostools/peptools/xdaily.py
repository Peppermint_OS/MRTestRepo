"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This is the Xdaily Gui layout application, its is used to manage the
* overy health of the system
"""

import tkinter as tk
import ttkbootstrap as ttk
import xdailyconf
import xdaily_functions


##### Set up the window and window icon ####
pexd = xdailyconf.bbstyle
pexd.resizable(False, False)
WINDOW_HEIGHT = 400
WINDOW_WIDTH = 650
pexd.tk.call('wm', 'iconphoto', pexd, tk.PhotoImage(
    file='/usr/share/pixmaps/peppermint-old.png'))
#############################################
##### Set the main notebook ####
main_notebook = ttk.Notebook(pexd, bootstyle="danger")
main_notebook.pack(expand=True, fill='both')
### Set the tabs in the notebook ###
xd_intro = ttk.Frame(main_notebook)
manage_updates = ttk.Frame(main_notebook)
system_maintenance = ttk.Frame(main_notebook)
### Tab label text ###
main_notebook.add(xd_intro, text='xDaily Introduction')
main_notebook.add(manage_updates, text='Manage Updates')
main_notebook.add(system_maintenance, text='System Maintenance')
### Binds for the tabs when they are clicked ###



# This one is for the system maint
system_maintenance.bind("<Button-1>", 
    lambda event: xdaily_functions.system_maintenance_tab(main_notebook, 
    output_text_maint,run_task_maintenance)
    )
###############################################

#### The in and out texts for the terminal piping ####
#### on the Manage updates tab ####
text_font = ("Helvetica", 10)
output_text = tk.Text(manage_updates, height=10, font=text_font, 
    wrap="word"
    )
output_text.place(x=200, y=135,width=430, height=200)
# on the system maint tab
output_text_maint = tk.Text(system_maintenance, height=10, font=text_font, 
    wrap="word"
    )
output_text_maint.place(x=200, y=135,width=430, height=200)
##################################################

##### These are the summary descriptions for the tabs and features####
xdaily_introduction_summary = ttk.Label(xd_intro,
                              text=(xdailyconf.INTRODUCTION_TEXT),
                              wraplength=600,
                              justify='left'
                              )
xdaily_introduction_summary.place(x=10, y=10)
manage_updates_description = ttk.Label(manage_updates,
                              text=(xdailyconf.CHECK_UPDATES_TEXT),
                              wraplength=430,
                              justify='left'
                              )
manage_updates_description.place(x=200, y=10)
system_maintenance_description = ttk.Label(system_maintenance,
    text=(xdailyconf.BROWSER_THUMBNAIL_CACHE_TEXT),
    wraplength=430, justify='left'
    )
system_maintenance_description.place(x=200, y=10)
#######################################################
#### The run buttons for each tab ####
#### Manage updates tave run  button ####
run_task_updates = ttk.Button(manage_updates, text="Run",
    bootstyle="danger",  cursor="hand2", width=5,
    command=lambda: xdaily_functions.run_apt_update(output_text,
        manage_updates
        )
    )
run_task_updates.place(x=562, y=100)
## System Maint run button ##
run_task_maintenance = ttk.Button(system_maintenance, text="Run",
    bootstyle="danger", cursor="hand2",  width=5
    )
run_task_maintenance.place(x=562, y=100)
########################################################
#########################################################

### xd_intro tab buttons ####
menu_xd_intro = ttk.Labelframe(xd_intro, bootstyle="dark",
    text="What would you like to do?"
    )
menu_xd_intro.place(x=5, y=150)
### Label frames for each tab ###
menu_manage_updates = ttk.Labelframe(manage_updates, bootstyle="dark",
    text="Tasks to run"
    )
menu_manage_updates.place(x=5, y=10)
menu_system_maintenance  = ttk.Labelframe(system_maintenance,
    bootstyle="dark", text="Tasks to run"
    )
menu_system_maintenance.place(x=5, y=10)
###########################################################

#### Buttons on the xDaily Tab ####
get_started = ttk.Button(menu_xd_intro, text="Get started",
    bootstyle="danger", cursor="hand2",
     command=lambda:xdaily_functions.manage_updates_tab(main_notebook, output_text, run_task_updates,xd_intro,manage_updates),
    width=8
    )
get_started.grid(row=1, column=0, ipadx=6, ipady=6, padx=6,
    pady=6,
    sticky='ew'
    )
pep_docs = ttk.Button(menu_xd_intro, text="Pep Docs",
    bootstyle="danger", cursor="hand2",
    command = xdaily_functions.pep_docs,
    width=8
    )
pep_docs.grid(row=1, column=1, ipadx=6, ipady=6, padx=6,
    pady=6, sticky='ew'
    )
community = ttk.Button(menu_xd_intro, text="Forums",
    bootstyle="danger", cursor="hand2",
    command = xdaily_functions.source_forge, width=8
    )
community.grid(row=2, column=0, ipadx=6, ipady=6, padx=6,
    pady=6, sticky='ew'
    )
runall = ttk.Button(menu_xd_intro, text="Run all tasks",
    bootstyle="danger", cursor="hand2",
    command = xdaily_functions.run_terminal_xdaily, width=8
    )
runall.grid(row=2, column=1, ipadx=6, ipady=6, padx=6, pady=6,
               sticky='ew'
               )
#####################################################
#### Buttons for the Manage Updates tab ####
check_for_updates = ttk.Button(menu_manage_updates,
    text="Check for Updates", bootstyle="danger", cursor="hand2",
    width=15,
    command= lambda: xdaily_functions.set_command_to_check_update(
        run_task_updates, output_text, manage_updates, xd_intro
        )
    )
check_for_updates.bind("<Button-1>", lambda event,
    label=manage_updates_description,
    text=xdailyconf.CHECK_UPDATES_TEXT: \
    xdaily_functions.update_label_text(label, text)
    )
check_for_updates.grid(row=1, column=0, ipadx=5, ipady=5, padx=5,
    pady=5, sticky='ew'
    )

#### Manage updates Tab buttons ####
view_updates_available  = ttk.Button(menu_manage_updates,
    text="View Updates", bootstyle="danger", cursor="hand2", width=15,
    command= lambda: xdaily_functions.set_command_to_view_update(
        run_task_updates, output_text, manage_updates, xd_intro
        )
    )
view_updates_available.grid(row=2, column=0, ipadx=5, ipady=5, padx=5,
    pady=5,sticky='ew'
    )
view_updates_available.bind("<Button-1>", lambda event,
    label=manage_updates_description,
    text=xdailyconf.VIEW_UPDATES_TEXT: \
    xdaily_functions.update_label_text(label, text,))

install_updates = ttk.Button(menu_manage_updates,
    text="Install Updates", bootstyle="danger", cursor="hand2",
    width=15,
    command= lambda: xdaily_functions.set_command_to_install_update(
        run_task_updates, output_text, manage_updates, xd_intro
        ) 
    )
install_updates.grid(row=3, column=0, ipadx=5, ipady=5, padx=5, pady=5,
                 sticky='ew'
                 )
install_updates.bind("<Button-1>", lambda event,
    label=manage_updates_description,
    text=xdailyconf.NSTALL_UPDATES_TEXT: \
    xdaily_functions.update_label_text(label, text)
    )
apt_clean = ttk.Button(menu_manage_updates, text="Run apt clean",
    bootstyle="danger", cursor="hand2", width=15,
    command= lambda: xdaily_functions.set_command_to_apt_clean(
        run_task_updates, output_text, manage_updates
        )
    )
apt_clean.grid(row=4, column=0, ipadx=5, ipady=5, padx=5, pady=5,
                 sticky='ew'
                 )
apt_clean.bind("<Button-1>", lambda event,
    label=manage_updates_description,
    text=xdailyconf.RUN_APT_CLEAN_TEXT: \
    xdaily_functions.update_label_text(label, text)
    )
apt_auto_clean = ttk.Button(menu_manage_updates,
    text="Run apt autoclean", bootstyle="danger", cursor="hand2",
    width=15,
    command= lambda: xdaily_functions.set_command_to_autoclean(
        run_task_updates, output_text, manage_updates
        ) 
    )
apt_auto_clean.grid(row=5, column=0, ipadx=5, ipady=5, padx=5, pady=5,
                 sticky='ew'
                 )
apt_auto_clean.bind("<Button-1>", lambda event,
    label=manage_updates_description,
    text=xdailyconf.RUN_APT_AUTOCLEAN_TEXT: \
    xdaily_functions.update_label_text(label, text)
    )
apt_auto_remove = ttk.Button(menu_manage_updates,
    text="Run apt autoremove", bootstyle="danger",
    cursor="hand2", width=15,
    command= lambda: xdaily_functions.set_command_to_autoremove(
        run_task_updates, output_text, manage_updates
        ) 
    )
apt_auto_remove.grid(row=6, column=0, ipadx=5, ipady=5, padx=5, pady=5,
                 sticky='ew'
                 )
apt_auto_remove.bind("<Button-1>",
    lambda event, label=manage_updates_description,
    text=xdailyconf.RUN_APT_AUTOREMOVE_TEXT:  \
    xdaily_functions.update_label_text(label, text)
    )
################################################
#### Button for the System Maint Tab ####
browser_thumbnail_cache = ttk.Button(menu_system_maintenance,
    text="Thumbnail Cache", bootstyle="danger", cursor="hand2",
    width=15,
    command= lambda: xdaily_functions.set_command_to_thumnail_cache(
        run_task_maintenance, output_text_maint, system_maintenance
        )
    )
    
browser_thumbnail_cache.bind("<Button-1>", lambda event,
    label=system_maintenance_description,
    text=xdailyconf.BROWSER_THUMBNAIL_CACHE_TEXT: \
    xdaily_functions.update_label_text(label, text)
    )
browser_thumbnail_cache.grid(row=1, column=0, ipadx=5, ipady=5, padx=5,
    pady=5, sticky='ew'
    )
clear_recently_used_list_firefox = ttk.Button(menu_system_maintenance,
    text="Recently Used", bootstyle="danger", cursor="hand2", width=15,
    command= lambda: xdaily_functions.set_command_recently_used(
        run_task_maintenance, output_text_maint, system_maintenance
    )
    
    )
clear_recently_used_list_firefox.bind("<Button-1>", lambda event,
    label=system_maintenance_description,
    text=xdailyconf.CLEAR_RECENTLYUSED_FIREFOX_TEXT: \
    xdaily_functions.update_label_text(label, text)
    )
clear_recently_used_list_firefox.grid(row=2, column=0, ipadx=5,
    ipady=5, padx=5, pady=5, sticky='ew'
    )
peppermint_branding = ttk.Button(menu_system_maintenance,
    text="Peppermint Branding", bootstyle="danger", cursor="hand2",
    width=15,
    command= lambda: xdaily_functions.set_command_peppermint_branding(
        run_task_maintenance, output_text_maint, system_maintenance
    )
    )
peppermint_branding.bind("<Button-1>", lambda event,
    label=system_maintenance_description,
    text=xdailyconf.PEPPERMINT_BRANDING_TEXT: \
    xdaily_functions.update_label_text(label, text)
    )
peppermint_branding.grid(row=3, column=0, ipadx=5, ipady=5, padx=5,
    pady=5, sticky='ew'
    )

ssd_trimming = ttk.Button(menu_system_maintenance, text="SSD TRIM",
    bootstyle="danger", cursor="hand2", width=15,
    command= lambda: xdaily_functions.set_command_ssd_trim(
        run_task_maintenance, output_text_maint, system_maintenance
    )
    )
ssd_trimming.bind("<Button-1>", lambda event,
    label=system_maintenance_description,
    text=xdailyconf.SSD_TRIMMING_TEXT: \
    xdaily_functions.update_label_text(label, text)
    )
ssd_trimming.grid(row=4, column=0, ipadx=5, ipady=5, padx=5, pady=5,
                 sticky='ew'
                 )
caching_icons = ttk.Button(menu_system_maintenance,
    text="Cache Icons", bootstyle="danger", cursor="hand2", width=15,
    command= lambda: xdaily_functions.set_caching_icons(
    run_task_maintenance, output_text_maint, system_maintenance
    )
    
    )
caching_icons.bind("<Button-1>",
    lambda event,
    label=system_maintenance_description,
    text=xdailyconf.CACHING_ICONS_TEXT:\
    xdaily_functions.update_label_text(label, text)
    )
caching_icons.grid(row=5, column=0, ipadx=5, ipady=5, padx=5, pady=5,
                 sticky='ew'
                 )
main_notebook.bind("<<NotebookTabChanged>>", lambda event, 
    run_task_updates=run_task_updates, output_text=output_text, 
    manage_updates=manage_updates, 
    xd_intro=xd_intro: xdaily_functions.on_manage_updates_clicked(
        event, run_task_updates, run_task_maintenance, 
        output_text, output_text_maint,  manage_updates, 
        system_maintenance, xd_intro)
    )

##################################################
### Start the functions ####
xdaily_functions.center_screen(pexd,WINDOW_HEIGHT,WINDOW_WIDTH)
xdaily_functions.wbase(pexd)
xdaily_functions.currently_pending_updates(xd_intro)
pexd.mainloop()

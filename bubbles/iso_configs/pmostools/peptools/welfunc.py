"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This file is used to keep functions that are used for the Welcome
* Screen
"""
import os.path
import shutil
# This will get the logged in user
gusr = os.getlogin()
spath = "/home/" + gusr + "/.local/share/pmostools/peptools"

def determine_browser(run_addrs):
    """ Determine what browser should be used by kumo"""

    lwpath = "/home/" + gusr + "/.local/share/pmostools/lw_profile"
    ffpath = "/home/" + gusr + "/.local/share/pmostools/ff_profile"
    if os.path.exists(lwpath) and os.path.isdir(lwpath):
        profile_path = os.path.expanduser(
        f"/home/{gusr}/.local/share/pmostools/lw_profile/"
        )
        command = (
        f'librewolf -P kumo --profile "{profile_path}" {run_addrs} &'
        )
        print("Found the LW, running LibreWolf")
    elif os.path.exists(ffpath) and os.path.isdir(ffpath):
        profile_path = os.path.expanduser(
        f"/home/{gusr}/.local/share/pmostools/ff_profile/"
        )
        
        if shutil.which('firefox'): 
            command = (
                f'firefox -P firefox_kumo --profile "{profile_path}" {run_addrs} &'
                )
            print("Found the FF, running Firefox")
        else:
            print("Firefox not found, running with luakit instead")
            command = f'luakit -U {run_addrs} &'
    else:
        command = f'luakit -U {run_addrs} &'
        print("This is a 32bit Flagship with no browser")
    return command


def suggested_packages():
    """ Open the suggested packages """
    os.system('python3 ' + spath + '/suggested.py')


def about():
    """Open the about page"""
    run_addrs = "https://peppermintos.com/about/"
    command = determine_browser(run_addrs)
    if command:
        os.system(command)
    else:
        print("There no suitebable browser installed!")


##SSB's###
def pep_docs():
    """Open Pep Docs"""
    run_addrs = "https://peppermint_os.codeberg.page/html/"
    command = determine_browser(run_addrs)
    if command:
        os.system(command)
    else:
        print("There no suitebable browser installed!")

# Community Section
def mastodon():
    """Open Mastodon"""
    run_addrs = "https://fosstodon.org/@peppermintos"
    command = determine_browser(run_addrs)
    if command:
        os.system(command)
    else:
        print("There no suitebable browser installed!")


def code_berg():
    """Open Codeberg"""
    run_addrs = "https://codeberg.org/Peppermint_OS"
    command = determine_browser(run_addrs)
    if command:
        os.system(command)
    else:
        print("There no suitebable browser installed!")


def matrix():
    """Open Matrix"""
    run_addrs = "https://matrix.to/#/!JhPtEbNexzFaoOkvGp:matrix.org?via=matrix.org"
    command = determine_browser(run_addrs)
    if command:
        os.system(command)
    else:
        print("There no suitebable browser installed!")

def source_forge():
    """Open Sourceforge"""
    run_addrs = "https://sourceforge.net/p/peppermintos/pepos/"
    command = determine_browser(run_addrs)
    if command:
        os.system(command)
    else:
        print("There no suitebable browser installed!")


def build_date():
    """Open BuildDate wiki"""
    run_addrs = "https://sourceforge.net/p/peppermintos/pepwiki/BuildDate/"
    command = determine_browser(run_addrs)
    if command:
        os.system(command)
    else:
        print("There no suitebable browser installed!")

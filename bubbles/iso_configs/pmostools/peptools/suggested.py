"""
* Author: PepDebian (peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This is for the suggested packages GUI
"""
import os
import subprocess
import tkinter as tk
import ttkbootstrap as ttk

# Create the main window
pwin = ttk.Window(themename="peppermint")
pwin.title("Suggested Packages")
pwin.resizable(False, False)
pwin.geometry('475x470')
pwin.tk.call('wm', 'iconphoto', pwin, tk.PhotoImage(file='/usr/share/pixmaps/peppermint-old.png'))

notebook = ttk.Notebook(pwin)
# Add some tabs to the notebook
tab1 = ttk.Frame(notebook)
tab2 = ttk.Frame(notebook)
notebook.add(tab1, text="Tab 1")
notebook.add(tab2, text="Tab 2")

# Function to check if certain software packages are installed
def check_packages():
    """ Check if software is installed """
    if os.path.exists('/usr/bin/atril'):
        pdf_viewer['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/snap'):
        snap['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/parole'):
        media_player['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/gufw'):
        fire_wall['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/flatpak'):
        flatpak['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/timeshift'):
        backup_tool['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/gnome-software'):
        store['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/keepassxc'):
        pwd_tool['state'] = tk.DISABLED

# Function to check if certain web browsers are installed
def check_web_browsers():
    """ Check what browsers are installed """
    if os.path.exists('/usr/bin/firefox'):
        fire_fox['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/konqueror'):
        kde_web['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/qutebrowser'):
        qute_web['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/falkon'):
        falkon_web['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/chromium'):
        chromium_web['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/epiphany-browser'):
        gnome_web['state'] = tk.DISABLED

# Functions to install packages
def install_package(package_name, check_function):
    """ Generic function to install a package and check installation status """
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install", package_name])
    check_function()

def install_atril():
    install_package("atril", check_packages)

def install_keepass():
    install_package("keepassxc", check_packages)

def install_parole():
    install_package("parole", check_packages)

def install_gufw():
    install_package("gufw", check_packages)

def install_snap():
    install_package("snapd", check_packages)

def install_flatpak():
    install_package("flatpak", check_packages)

def install_store():
    install_package("gnome-software", check_packages)

def install_timeshift():
    install_package("timeshift", check_packages)

def install_firefox():
    install_package("firefox-esr", check_web_browsers)

def install_konqueror():
    install_package("konqueror", check_web_browsers)

def install_epiphany():
    install_package("epiphany-browser", check_web_browsers)

def install_tor():
    install_package("torbrowser-launcher", check_web_browsers)

def install_qute():
    install_package("qutebrowser", check_web_browsers)

def install_chromium():
    install_package("chromium", check_web_browsers)

def install_falkon():
    install_package("falkon", check_web_browsers)

# Function to set up the software installation options based on the OS
def check_base_snaps():
    """ If Devuan is loaded do not show snaps """
    global flatpak, store, backup_tool, pwd_tool, snap

    if os.path.exists("/etc/devuan_version"):
        flatpak = ttk.Button(software_frame, text="Flatpak Package Platform", cursor="hand2", style="danger", command=install_flatpak)
        flatpak.grid(row=4, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')
        
        store = ttk.Button(software_frame, text="Gnome Software Store", cursor="hand2", style="danger", command=install_store)
        store.grid(row=5, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')
        
        backup_tool = ttk.Button(software_frame, text="TimeShift: Backup Tool", cursor="hand2", style="danger", command=install_timeshift)
        backup_tool.grid(row=6, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')
        
        pwd_tool = ttk.Button(software_frame, text="KeePassXC: Password Tool", cursor="hand2", style="danger", command=install_keepass)
        pwd_tool.grid(row=7, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')
    elif os.path.exists("/etc/debian_version"):
        snap = ttk.Button(software_frame, text="Snap Package Platform", cursor="hand2", style="danger", command=install_snap)
        snap.grid(row=4, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')
        
        flatpak = ttk.Button(software_frame, text="Flatpak Package Platform", cursor="hand2", style="danger", command=install_flatpak)
        flatpak.grid(row=5, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')
        
        store = ttk.Button(software_frame, text="Gnome Software Store", cursor="hand2", style="danger", command=install_store)
        store.grid(row=6, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')
        
        backup_tool = ttk.Button(software_frame, text="TimeShift: Backup Tool", cursor="hand2", style="danger", command=install_timeshift)
        backup_tool.grid(row=7, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

# Create and organize the layout
fsw = ttk.Frame(pwin, width=200)
fsw.grid(row=2, column=0, columnspan=4)
software_frame = ttk.Labelframe(fsw, bootstyle="dark", text="Suggested Software")
software_frame.grid(row=5, column=0, columnspan=2, ipadx=0, ipady=0, padx=10, pady=10)

# Buttons for the suggested software
pdf_viewer = ttk.Button(software_frame, text="Atril: a document viewer", cursor="hand2", style="danger", command=install_atril)
pdf_viewer.grid(row=0, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

media_player = ttk.Button(software_frame, text="Parole: Media Player", cursor="hand2", style="danger", command=install_parole)
media_player.grid(row=2, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

fire_wall = ttk.Button(software_frame, text="gufw: GNU Firewall", cursor="hand2", style="danger", command=install_gufw)
fire_wall.grid(row=3, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

# Frame that manages the Browser buttons
fs = ttk.Frame(pwin)
fs.grid(row=2, column=5, columnspan=2, ipadx=0, ipady=0, padx=10, pady=10)

# Frame Title
soc = ttk.Labelframe(fs, bootstyle="dark", text="Suggested Web Browsers")
soc.grid(row=5, column=0, columnspan=2, ipadx=0, ipady=0, padx=10, pady=10)

# Web Browser Buttons
fire_fox = ttk.Button(soc, text="Firefox ESR Browser", cursor="hand2", style="danger", command=install_firefox)
fire_fox.grid(row=0, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

kde_web = ttk.Button(soc, text="Konqueror KDE Browser", cursor="hand2", style="danger", command=install_konqueror)
kde_web.grid(row=1, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

gnome_web = ttk.Button(soc, text="Gnome Web (Epiphany)", cursor="hand2", style="danger", command=install_epiphany)
gnome_web.grid(row=2, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

tor_web = ttk.Button(soc, text="Tor Browser: Privacy", cursor="hand2", style="danger", command=install_tor)
tor_web.grid(row=3, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

qute_web = ttk.Button(soc, text="Qutebrowser: Vim-like", cursor="hand2", style="danger", command=install_qute)
qute_web.grid(row=4, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

chromium_web = ttk.Button(soc, text="Chromium Browser", cursor="hand2", style="danger", command=install_chromium)
chromium_web.grid(row=5, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

falkon_web = ttk.Button(soc, text="Falkon Qt Browser", cursor="hand2", style="danger", command=install_falkon)
falkon_web.grid(row=6, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

lblpmtitle = ttk.Label(pwin, style="F62817.TLabel", text='These suggestions are listed in the official repositories')
lblpmtitle.place(x=5, y=440)

check_base_snaps()
check_packages()
check_web_browsers()
pwin.mainloop()

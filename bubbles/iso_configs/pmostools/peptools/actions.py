"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* this script is for performing actions for the hub
*
"""
import os
import subprocess

# This will get the logged in user
gusr = os.getlogin()
spath = "/home/" + gusr + "/.local/share/pmostools/peptools"


def osettingsmgr():
    """#Settings Manager"""
    subprocess.Popen("xfce4-settings-manager")


def nhnm():
    """#Network Manager"""
    subprocess.Popen("nm-connection-editor")


def nhpr():
    """#Printers"""
    subprocess.Popen("system-config-printer")

def nhpav():
    """#Pulse Audio Volume"""
    subprocess.Popen("pavucontrol")


def ssug():
    """#User and  Groups"""
    subprocess.Popen("users-admin")


def ssdu():
    """#Disk Utilities"""
    subprocess.Popen("gnome-disks")

def sssi():
    """#System Information"""
    cmd = "x-terminal-emulator -e 'bash -c \"neofetch\";bash'"
    subprocess.Popen(cmd, shell=True)

def ssst():
    """#Snapstore"""
    cmd = "[ -e /snap/bin ] && snap-store || x-terminal-emulator -e \"bash -c 'sudo snap install snap-store'\""
    subprocess.Popen(cmd, shell=True)


def ssfh():
    """#Flathub"""
    os.system('luakit -U https://flathub.org &')


def ssss():
    """#Snapstore"""
    os.system('luakit -U https://snapcraft.io/store &')

def ssai():
    """#App Image"""
    os.system('luakit -U https://www.appimagehub.com/ &')

def ssgn():
    """#gnomestore"""
    subprocess.Popen("gnome-software")


def ssgns():
    """#gnomestoreweb"""
    os.system('luakit -U https://apps.gnome.org/ &')


def ssspm():
    """#Synaptic Package Manage"""
    subprocess.Popen("synaptic-pkexec")

def packages():
    """#Installer"""
    os.system('python3 ' + spath + '/suggested.py')


"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* this files should be used for the over all style and design of
* the Welcome Screen
"""
from tkinter import PhotoImage
import ttkbootstrap as ttk
import os

# This will set the style to used for boostrap
# just change the name to what is needed for the
# the system
bbstyle = ttk.Window(themename="darkly")

# set the title of the window
DEBIAN_TITLE = "Upgrade Peppermint - (Debian)"
DEVUAN_TITLE = "U@grade Peppermint - (Devuan)"

# This will get the logged in user
gusr = os.getlogin()
spath = "/home/" + gusr + "/.local/share/pmostools/peptools"









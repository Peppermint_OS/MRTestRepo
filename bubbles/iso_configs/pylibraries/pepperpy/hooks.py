"""
* Author: "PeppermintOS Team(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This hook is the central one used for all the builds
* Add things as needed
"""
import os
import platform


def apt_install_packages():
    """ 
    The place where installing apt packaged is preffered during the
    hook run ran ther then the general build
    """
    os.system('apt update')
    os.system('apt install --yes peppermint-wallpapers')


def remove_packages_from_the_build():
    """
    Uninstall unwanted packages from the build
    """
    packages = [
        'konsole',
        'malcontent',
        'systemsettings',
        'nvidia*',
        'imagemagick-6.q16'
    ]
    
    for package in packages:
        os.system(f'apt --purge --yes autoremove {package}')


def remmove_desktop_files():
    """
    Use this def to remove any .desktop files you donot need
    """
    os.system('rm /usr/share/applications/btop.desktop')
    os.system('rm /usr/share/applications/librewolf.desktop')


def install_librewolf_browser():
    """
    Installs LibreWolf browser for Kumo by setting up sources and 
    installing the package.
    """
    path_to_file = '/opt/pepconf/'
    build_id_list = [
        path_to_file + 'deb.64xfc',
        path_to_file + 'dev.64xfc',
        path_to_file + 'deb.armxfc',
        path_to_file + 'dev.armxfc',
        path_to_file + 'deb.64gfb',
        path_to_file + 'dev.64gfb',
        path_to_file + 'deb.64opb',
        path_to_file + 'dev.64opb'
    ]
    distro_cmd = (
        "distro=$(if echo ' una bookworm vanessa focal jammy bullseye vera uma ' | "
        "grep -q ' $(lsb_release -sc) '; then lsb_release -sc; else echo bookworm; fi); "
        "echo $distro"
    )
    for build_id in build_id_list:
        if os.path.exists(build_id):
            distro = os.popen(distro_cmd).read().strip()
            content = (
                f"Types: deb\n"
                f"URIs: https://deb.librewolf.net\n"
                f"Suites: {distro}\n"
                f"Components: main\n"
                f"Architectures: amd64\n"
                f"Signed-By: /usr/share/keyrings/librewolf.gpg\n"
            )

            file_path = '/etc/apt/sources.list.d/librewolf.sources'
            os.system('apt update && '
                      'apt install -y wget gnupg lsb-release apt-transport-https ca-certificates')
            os.system('wget -O- https://deb.librewolf.net/keyring.gpg | '
                      'gpg --dearmor -o /usr/share/keyrings/librewolf.gpg')
            try:
                with open(file_path, 'w',  encoding='utf-8') as f:
                    f.write(content)
                os.chmod(file_path, 0o644)
            except PermissionError:
                print(f"Permission denied. Please run this script with "
                      f"appropriate permissions to write to {file_path}.")
                return
            os.system('apt update')
            os.system('apt install librewolf -y')
            print("Browser installed!")
        else:
            print("No builds were met not installed")


def setup_plymouth_theme_grub():
    """
    Set the plymouth boot theme and desktop theme, as well as grub
    """
    os.system('plymouth-set-default-theme -R joy')
    os.system('rm -f /etc/alternatives/desktop-theme')
    os.system('rm -f /etc/alternatives/desktop-grub')

    theme_source = "/usr/share/desktop-base/joy-theme"
    theme_destination = "/etc/alternatives/desktop-theme"
    theme_command = f"ln -s {theme_source} {theme_destination}"
    os.system(theme_command)

    grub_source = (
        "/usr/share/desktop-base/active-theme/grub/"
        "pep-grub-16x9.png"
        )
    grub_destination = "/etc/alternatives/desktop-grub"
    grub_command = f"ln -s {grub_source} {grub_destination}"
    os.system(grub_command)


def hblock_setup():
    """
    Install hblock to the system
    """
    hb_url = "https://raw.githubusercontent.com/hectorm/hblock/master/hblock"
    hb_output = "/tmp/hblock"
    hb_command = f"curl {hb_url} --output {hb_output}"
    os.system(hb_command)
    file_path = "/tmp/hblock"
    expected_hash = "4031d86cd04fd7c6cb1b7e9acb1ffdbe9a3f84f693bfb287c68e1f1fa2c14c3b"
    sh_command = f'[ "$(sha256sum {file_path})" == "{expected_hash}" ]'
    os.system(sh_command)
    os.system('mv /tmp/hblock /usr/local/bin/hblock')
    os.system('chown 0:0 /usr/local/bin/hblock')
    os.system('chmod 755 /usr/local/bin/hblock')
    os.system('hblock -S none -D none exit 0')

## this function may not be necessary because the bug is already fixed in live-build.
def manage_raspi_firmware():
    """
    Manage when the raspi firmware, is removed.
    """
    if os.path.exists("/usr/share/peppermint/pep_id"):
        os.system("apt remove raspi-firmware -y")
        os.system("apt purge raspi-firmware -y")
        os.system("apt autoremove -y")
    else:
        print("not there")


def manage_symlinks():
    """
    Symlink management 
    """
    # Workaround for using snapd on debian.  Thank You - @stevesveryown.
    os.system('[ ! -e /etc/skel/.local/share ] && mkdir -p /etc/skel/.local/share')
    os.system('ln -s snap /usr/bin/snap-store')
    # These are for the 3 installer scripts - icons.sh, themes.sh & walls.sh
    # In 0610- we set the permissions to 777 on the DIRECTORY, not the contents.
    os.system('ln -s icons /usr/share/pepicons')
    os.system('ln -s themes /usr/share/pepthemes')
    os.system('ln -s backgrounds /usr/share/pepwallpaper')


def set_vi_vim():
    """
    Setting the better version of vim
    """
    # A better version of vi and vim than vim-tiny
    os.system('rm /etc/alternatives/vi')
    os.system('ln -s ../../etc/alternatives/vim /usr/bin/vim')
    os.system('ln -s ../../usr/bin/vim.tiny /etc/alternatives/vim')
    os.system('ln -s ../../usr/bin/busybox  /etc/alternatives/vi')


def setting_permissions():
    """
     This allows write access to the Welcome Screen database to users
     in the "cdrom" group. IF this a system wide file, one user can
     disable or turn it off for *ALL* accounts. If this is meant to be
     on a per user basis, put the .db in ${HOME}/.config . Or rewrite
     welcome.py to write to the .db WHICH user requested to disable
     this feature. Set Permissions on desktop files
     """
     # Desktop links
    os.system('chmod 755 /usr/share/applications/Welcome.desktop')
    os.system('chmod 755 /usr/share/applications/plank.desktop')
    os.system('chmod 755 /usr/share/applications/kumo.desktop')
    os.system('chmod 755 /usr/share/applications/calamares-install-peppermint.desktop')
    # Executables
    os.system('chmod 755 /usr/local/bin/xDaily')
    os.system('chmod 755 /usr/local/bin/kumo')
    os.system('chmod 755 /usr/local/bin/welcome')
    os.system('chmod 755 /usr/bin/install-peppermint')
    os.system('chmod 755 /opt/pepconf/*')
    # Set permissions on the Python libs
    os.system('chmod 755 -R /usr/lib/python3/dist-packages/tendo')
    os.system('chmod 755 -R /usr/lib/python3/dist-packages/tendo-0.3.0.dist-info')
    os.system('chmod 755 -R /usr/lib/python3/dist-packages/ttkbootstrap')
    os.system('chmod 755 -R /usr/lib/python3/dist-packages/ttkbootstrap-1.10.1.dist-info')
    os.system('chmod 755 -R /usr/lib/python3/dist-packages/ttkcreator')
    # Set permissions on the lines theme
    os.system('chmod 755 -R /usr/share/desktop-base/lines-theme')
    # Set Grub Themes Permission
    os.system('chmod 755 -R /boot/grub/themes')
    # Set the Calamares Permissions]
    os.system('chmod 755 -R /etc/calamares')
    ### After installation, additional groups to add new users to.
    os.system('grep -B99 "#EXTRA_GROUPS=" /etc/adduser.conf' +
        '>  /etc/adduser.conf.new')
    os.system('grep      "#EXTRA_GROUPS=" /etc/adduser.conf' +
              ' | cut -c2-' + 
              '>> /etc/adduser.conf.new'
              )
    os.system('grep -B3  "#ADD_EXTRA_GROUPS=" /etc/adduser.conf        >> /etc/adduser.conf.new')
    os.system('grep      "#ADD_EXTRA_GROUPS=" /etc/adduser.conf | cut -c2- >> /etc/adduser.conf.new')
    os.system('grep -B3  "#NAME_REGEX=" /etc/adduser.conf          >> /etc/adduser.conf.new')
    # These are for the 3 installer scripts - icons.sh, themes.sh & walls.sh
    # In 0600- we created symlinks to these directories. This makes them writable.
    os.system('chmod 755 -R /usr/share/icons')
    os.system('chmod 755 -R /usr/share/themes')
    os.system('chmod 755 -R /usr/share/backgrounds')
    os.system('chmod 755 -R /usr/share/pixmaps')


def final_cleanup():
    """
    This section is the "lint-trap" to remove files and/or
    directories not associated with or required by PepOS.
    Followed by "\" , add files to be removed, one per line.
    """
    files_to_remove = [ "/usr/bin/install-debian",
                        "/usr/share/applications/calamares-install-debian.desktop"
                       ]
    for file_path in files_to_remove:
        if os.path.exists(file_path):
            os.system(f"rm -rf {file_path}")
            print(f"   Removed: {file_path}")
        else:
            print(f"   The path {file_path} was not found and couldn't be removed.")
    ### This might be better in an OS-tweaks hook script.
    # Lowers the footprint in RAM by 200 MB at the small expense of added size to the ISO.
    os.system('update-icon-caches /usr/share/icons/*')
    ### Setting --apt-recommends and --apt-suggests defaults to '0'
    #os.system('echo -e "# Changing these values to \"1\" may quickly fill up a small partition" > /etc/apt/apt.conf.d/99No-Recommends')
    #os.system('echo -e "# Changing these values to \\"1\\" may quickly fill up a small partition" > /etc/apt/apt.conf.d/99No-Recommends')
    #os.system('echo -e "APT::Install-Recommends \"0\";\nAPT::Install-Suggests \"0\";" >> /etc/apt/apt.conf.d/99No-Recommends')
    #os.system('echo -e "APT::Install-Recommends \\"0\\";\nAPT::Install-Suggests \\"0\\";" >> /etc/apt/apt.conf.d/99No-Recommends')
    ### Fixes the "Not installing grub for UEFI Secure Boot" in all versions , after the default was changed.
    #sed s/keyutils/"keyutils --install-recommends"/ /usr/sbin/bootloader-config > /tmp/bootloader-config
    os.system('echo -e "$(grep -A1 -B20 "Installing grub-efi (uefi)..." /usr/sbin/bootloader-config) --install-recommends" > /tmp/bootloader-config')
    os.system('echo -e "$(grep -A2 "else" /usr/sbin/bootloader-config) --install-recommends\nfi" >> /tmp/bootloader-config')
    os.system('chmod +x /tmp/bootloader-config && mv /tmp/bootloader-config /usr/sbin/bootloader-confi')


def rename_kernel():
    """
    These commands are typically used in the context of updating the
    initial ramdisk image and the kernel image for a live Linux system.
    The steps can be summarized as follows:
    Removal of the current boot files: initrd.img and vmlinuz are
    removed to clear out the old versions.
    Copying the updated versions: New versions of these files,
    which match the patterns initrd.img-* and vmlinuz-*, are copied
    to the standard names initrd.img and vmlinuz.
    """
    os.system('rm live/initrd.img')
    os.system('rm live/vmlinuz')
    os.system('cp live/initrd.img-* live/initrd.img')
    os.system('cp live/vmlinuz-* live/vmlinuz')


def start():
    """ This will begin the execution process of the hooks
        First see what arch is being ran and run the correct
        functions
    """
    architecture = platform.architecture()[0]
    if '64bit' in architecture:
        apt_install_packages()
        remove_packages_from_the_build()
        install_librewolf_browser()
        remmove_desktop_files()
        setup_plymouth_theme_grub()
        hblock_setup()
        manage_raspi_firmware()
        manage_symlinks()
        set_vi_vim()
        setting_permissions()
        final_cleanup()
        rename_kernel()
    else:
        apt_install_packages()
        remmove_desktop_files()
        remove_packages_from_the_build()
        setup_plymouth_theme_grub()
        hblock_setup()
        manage_raspi_firmware()
        manage_symlinks()
        set_vi_vim()
        setting_permissions()
        final_cleanup()
        rename_kernel()


start()

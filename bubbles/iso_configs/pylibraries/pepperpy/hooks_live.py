"""
* Author: "PeppermintOS Team(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This hook is the central one used for the live session
* Add things as needed
"""
import os
import subprocess

def cmdline():
    """ Function to read kernel command line parameters """
    global LIVE_USER_DEFAULT_GROUPS, LIVE_USER_FULLNAME, LIVE_USERNAME
    LIVE_USER_DEFAULT_GROUPS = ""
    LIVE_USER_FULLNAME = ""
    LIVE_USERNAME = ""

    # Read kernel command line parameters
    with open("/proc/cmdline", "r") as f:
        cmdline_params = f.read().split()

    for param in cmdline_params:
        if "live-config.user-default-groups=" in param or "user-default-groups=" in param:
            LIVE_USER_DEFAULT_GROUPS = param.split('=')[-1]
        elif "live-config.user-fullname=" in param or "user-fullname=" in param:
            LIVE_USER_FULLNAME = param.split('=')[-1]
        elif "live-config.username=" in param or "username=" in param:
            LIVE_USERNAME = param.split('=')[-1]


def pkg_is_installed(pkg_name):
    """ Function to check if a package is installed """
    result = subprocess.run(['dpkg', '-s', pkg_name], stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    return result.returncode == 0


def component_was_executed(component_name):
    """ Function to check if a component was executed """
    return os.path.exists(f"/var/lib/live/config/{component_name}")


def init():
    """ Initialization function """
    if not pkg_is_installed("user-setup") or component_was_executed("user-setup"):
        exit(0)
    print(" user-setup", end='')


def config():
    """ Configuration function """
    global LIVE_USER_DEFAULT_GROUPS

    # Check if user is already configured
    with open("/etc/passwd", "r") as f:
        if any(LIVE_USERNAME in line for line in f):
            exit(0)

    # Adjust formatting of groups
    if LIVE_USER_DEFAULT_GROUPS:
        LIVE_USER_DEFAULT_GROUPS = LIVE_USER_DEFAULT_GROUPS.replace(",", " ")

    # Make sure user is not in sudo group if sudo is disabled
    if os.getenv("LIVE_CONFIG_NOROOT") == "true":
        LIVE_USER_DEFAULT_GROUPS = LIVE_USER_DEFAULT_GROUPS.replace("sudo", "")

    # Default password is 'U6aMy0wojraho' (blank password)
    _PASSWORD = "U6aMy0wojraho"

    # Write configuration to temporary file
    with open("/tmp/live-config.cfg", "w") as f:
        f.write(f"""user-setup passwd/make-user boolean true
    user-setup passwd/root-password-crypted string *
    user-setup passwd/user-password-crypted string {_PASSWORD}
    user-setup passwd/user-default-groups string {LIVE_USER_DEFAULT_GROUPS}
    user-setup passwd/user-fullname string {LIVE_USER_FULLNAME}
    user-setup passwd/username string {LIVE_USERNAME}
    user-setup passwd/user-uid string 1000
    """)

    # Apply configuration
    subprocess.run(['debconf-set-selections', '<', '/tmp/live-config.cfg'],
        shell=True)
    os.remove("/tmp/live-config.cfg")

    # Workaround for bug in shadow
    result = subprocess.run(['/usr/lib/user-setup/user-setup-apply'],
        stderr=subprocess.PIPE)
    for line in result.stderr.decode().split('\n'):
        if "Shadow passwords are now on" not in line:
            print(line)

    # Creating state file
    with open("/var/lib/live/config/user-setup", "w") as f:
        f.write("")


def set_plymouth():
    """
    1. set the plymouth on boot
    2. Remove the defauly theme
    3. Re added the new default theme
    """
    source = '/usr/share/desktop-base/joy-theme'
    destination = '/etc/alternatives/desktop-theme'

    os.system('plymouth-set-default-theme -R joy')
    os.system('rm -f /etc/alternatives/desktop-theme')
    os.system(f'ln -sf {source} {destination}')


def set_installer_permissions():
    """
    set permmissions needed for general install
    """
    source = '/usr/share/desktop-base/joy-theme'
    destination = '/etc/alternatives/desktop-theme'
    makelnk = f'ln -sf {source} {destination}'

    os.system('chmod 755 /usr/bin/install-peppermint')
    os.system('chmod 755 /usr/share/applications/calamares-install-peppermint.desktop')
    os.system(makelnk)
    os.system('chmod 755 -R /boot/grub/themes')
    os.system('cd /opt')
    os.system('curl -LO https://calamares.io/deploycala.py')
    os.system('python3 deploycala.py')


def other_misc_settings():
    """
    Other settings as needed
    """
    source ="/usr/share/desktop-base/active-theme/grub/pep-grub-16x9.png"
    destination = "/etc/alternatives/desktop-grub"
    makelnk = f'ln -sf {source} {destination}'
    # Remove the grub symlink for the background
    os.system('rm -f /etc/alternatives/desktop-grub')
    # Then recreate it with the Pep Background
    os.system(makelnk)

def remmove_desktop_files():
    """
    Use this def to remove any .desktop files you donot need
    """
    os.system('rm /usr/share/applications/btop.desktop')

# Main execution
cmdline()
init()
config()
set_plymouth()
set_installer_permissions()
remmove_desktop_files()

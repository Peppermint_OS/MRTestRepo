// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1718865650);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1718865650);
user_pref("app.update.lastUpdateTime.services-settings-poll-changes", 1718865650);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 1718865650);
user_pref("browser.bookmarks.addedImportButton", true);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.contentblocking.category", "strict");
user_pref("browser.dom.window.dump.enabled", false);
user_pref("browser.download.viewableInternally.typeWasRegistered.avif", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.webp", true);
user_pref("browser.migration.version", 143);
user_pref("browser.newtabpage.activity-stream.impressionId", "{0a8db3dd-c27e-48ba-a150-b4e3b480e338}");
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pageActions.persistedActions", "{\"ids\":[\"bookmark\"],\"idsInUrlbar\":[\"bookmark\"],\"idsInUrlbarPreProton\":[],\"version\":1}");
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.policies.applied", true);
user_pref("browser.policies.runOncePerModification.extensionsInstall", "[\"https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi\"]");
user_pref("browser.policies.runOncePerModification.extensionsUninstall", "[\"google@search.mozilla.org\",\"bing@search.mozilla.org\",\"amazondotcom@search.mozilla.org\",\"ebay@search.mozilla.org\",\"twitter@search.mozilla.org\"]");
user_pref("browser.policies.runOncePerModification.removeSearchEngines", "[\"Google\",\"Bing\",\"Amazon.com\",\"eBay\",\"Twitter\"]");
user_pref("browser.policies.runOncePerModification.setDefaultSearchEngine", "DuckDuckGo");
user_pref("browser.proton.toolbar.version", 3);
user_pref("browser.region.network.url", "");
user_pref("browser.region.update.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
user_pref("browser.safebrowsing.downloads.remote.block_uncommon", false);
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.url", "");
user_pref("browser.safebrowsing.provider.google4.dataSharingURL", "");
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1718865536293");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1718887136293");
user_pref("browser.startup.couldRestoreSession.count", 1);
user_pref("browser.startup.lastColdStartupCheck", 1718866696);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"unified-extensions-area\":[],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"customizableui-special-spring1\",\"urlbar-container\",\"customizableui-special-spring2\",\"save-to-pocket-button\",\"downloads-button\",\"fxa-toolbar-menu-button\",\"unified-extensions-button\",\"ublock0_raymondhill_net-browser-action\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"import-button\",\"personal-bookmarks\"]},\"seen\":[\"developer-button\",\"ublock0_raymondhill_net-browser-action\"],\"dirtyAreaCache\":[\"nav-bar\",\"PersonalToolbar\",\"toolbar-menubar\",\"TabsToolbar\"],\"currentVersion\":20,\"newElementCount\":2}");
user_pref("browser.urlbar.placeholderName", "DuckDuckGo");
user_pref("captivedetect.canonicalURL", "");
user_pref("devtools.console.stdout.chrome", false);
user_pref("devtools.debugger.remote-enabled", false);
user_pref("distribution.iniFile.exists.appversion", "125.0.3-1");
user_pref("distribution.iniFile.exists.value", false);
user_pref("doh-rollout.provider-list", "[{\"UIName\":\"Mozilla Cloudflare\",\"uri\":\"https://mozilla.cloudflare-dns.com/dns-query\"},{\"UIName\":\"Quad9\",\"uri\":\"https://dns.quad9.net/dns-query\"}]");
user_pref("dom.forms.autocomplete.formautofill", true);
user_pref("dom.push.userAgentID", "cf136d08c3164437bc7c322182502e21");
user_pref("dom.security.https_only_mode_ever_enabled", true);
user_pref("extensions.activeThemeID", "default-theme@mozilla.org");
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.databaseSchema", 35);
user_pref("extensions.getAddons.cache.lastUpdate", 1718865651);
user_pref("extensions.getAddons.databaseSchema", 6);
user_pref("extensions.lastAppBuildId", "20240502032855");
user_pref("extensions.lastAppVersion", "125.0.3-1");
user_pref("extensions.lastPlatformVersion", "125.0.3");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.pictureinpicture.enable_picture_in_picture_overrides", true);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.webcompat.enable_shims", true);
user_pref("extensions.webcompat.perform_injections", true);
user_pref("extensions.webcompat.perform_ua_overrides", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.screenshots@mozilla.org", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.uBlock0@raymondhill.net", true);
user_pref("extensions.webextensions.uuids", "{\"formautofill@mozilla.org\":\"60697a35-6ffa-4f5e-bb92-272c67731adb\",\"pictureinpicture@mozilla.org\":\"4b3e6970-4b7d-4cff-b622-9759d793084b\",\"screenshots@mozilla.org\":\"eb46a54c-c827-47e5-b66a-ff83c3fc8b93\",\"webcompat@mozilla.org\":\"52529f44-2de0-47e0-b08f-973b770cca79\",\"default-theme@mozilla.org\":\"9c4ed647-aa9f-42a8-8e96-197e8c4a8713\",\"addons-search-detection@mozilla.com\":\"2c2561d4-d08d-4edf-945d-679cbe563c28\",\"wikipedia@search.mozilla.org\":\"a289e5f0-0417-4afc-a23a-3ff435a12f21\",\"ddg@search.mozilla.org\":\"a86b4320-0587-43c5-a5b8-b2959f06e4a7\",\"uBlock0@raymondhill.net\":\"d19e9620-41c2-466d-8565-f7e9bbd1b705\"}");
user_pref("gecko.handlerService.defaultHandlersVersion", 1);
user_pref("intl.accept_languages", "en-US, en");
user_pref("javascript.use_us_english_locale", true);
user_pref("media.gmp.storage.version.observed", 1);
user_pref("network.captive-portal-service.enabled", false);
user_pref("network.connectivity-service.enabled", false);
user_pref("network.http.referer.disallowCrossSiteRelaxingDefault.top_navigation", true);
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("network.predictor.enabled", false);
user_pref("network.prefetch-next", false);
user_pref("pdfjs.enabledCache.state", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("permissions.delegation.enabled", false);
user_pref("permissions.manager.defaultsUrl", "");
user_pref("privacy.annotate_channels.strict_list.enabled", true);
user_pref("privacy.fingerprintingProtection", true);
user_pref("privacy.history.custom", true);
user_pref("privacy.query_stripping.enabled", true);
user_pref("privacy.query_stripping.enabled.pbmode", true);
user_pref("privacy.sanitize.pending", "[]");
user_pref("privacy.trackingprotection.emailtracking.enabled", true);
user_pref("privacy.trackingprotection.enabled", true);
user_pref("privacy.trackingprotection.socialtracking.enabled", true);
user_pref("security.sandbox.content.tempDirSuffix", "29075e46-f4aa-4d36-b1da-e8c8cfcd2710");
user_pref("security.tls.enable_0rtt_data", false);
user_pref("services.sync.engine.addresses.available", true);
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
user_pref("toolkit.startup.last_success", 1718866695);
user_pref("toolkit.telemetry.cachedClientID", "e133d4af-a772-4264-86bc-618be65074d6");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("toolkit.winRegisterApplicationRestart", false);
user_pref("webchannel.allowObject.urlWhitelist", "");

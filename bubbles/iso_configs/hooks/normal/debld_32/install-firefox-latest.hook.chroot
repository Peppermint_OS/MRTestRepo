#!/bin/bash

# Script to be used as a hook in Debian Live-Build
# to install Firefox from the official Mozilla repository along with all available language packs

# Function to set up the Mozilla repository and GPG key
setup_repository_and_key() {
    echo "Setting up Mozilla repository and GPG key for Firefox..."

    # Create directory to store the APT repository keyring if it doesn't exist
    install -d -m 0755 /etc/apt/keyrings

    # Import the Mozilla APT repository authentication key
    wget -q https://packages.mozilla.org/apt/repo-signing-key.gpg -O- | tee /etc/apt/keyrings/packages.mozilla.org.asc > /dev/null

    # Verify the GPG key fingerprint
    expected_fingerprint="35BAA0B33E9EB396F59CA838C0BA5CE6DC6315A3"
    actual_fingerprint=$(gpg -n -q --import --import-options import-show /etc/apt/keyrings/packages.mozilla.org.asc | awk '/pub/{getline; gsub(/^ +| +$/,""); print $0}')

    if [ "$actual_fingerprint" == "$expected_fingerprint" ]; then
        echo "The GPG key fingerprint matches: $actual_fingerprint."
    else
        echo "Verification failed: the GPG key fingerprint ($actual_fingerprint) does not match the expected one ($expected_fingerprint)."
        exit 1
    fi

    # Add the Mozilla APT repository to the sources list
    echo "deb [signed-by=/etc/apt/keyrings/packages.mozilla.org.asc] https://packages.mozilla.org/apt mozilla main" > /etc/apt/sources.list.d/mozilla.list

    # Configure APT to prioritize packages from the Mozilla repository
    echo '
Package: *
Pin: origin packages.mozilla.org
Pin-Priority: 1000
' > /etc/apt/preferences.d/mozilla

    # Update the package list
    apt-get update
}

# Function to install Firefox and available language packs
install_firefox() {
    echo "Installing Firefox and its language packs..."

    # Install Firefox
    apt-get install -y firefox

    # Install all available Firefox language packs
    apt-get install -y $(apt-cache search firefox-l10n | awk '{print $1}')
}

# Main execution of the script
main() {
    setup_repository_and_key
    install_firefox

    echo "Firefox installation completed."
}

# Execute the main function
main


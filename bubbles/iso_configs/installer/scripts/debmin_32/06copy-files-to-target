#!/bin/sh -e

# SPDX-License-Identifier: GPL-3.0-or-later
#
# SPDX-FileCopyrightText:  2023 PeppermintOS Team  (peppermintosteam@proton.me)

# This script copies a file to the chroot and runs commands during the Debian installation process.

# setup a folder structure
mkdir -p /target/etc/skel/.local/share/pmostools
mkdir -p /target/etc/skel/.config/autostart
mkdir -p /target/opt/pepconf
mkdir -p /target/opt/pypep/dbpep
mkdir -p /target/etc/default
mkdir -p /target/etc/apt/sources.list.d
mkdir -p /target/etc/apt
mkdir -p /target/etc/apt/trusted.gpg.d
mkdir -p /target/usr/share/keyrings
mkdir -p /target/usr/share/applications
mkdir -p /target/opt/pepconf
mkdir -p /target/opt/pypep/dbpep
mkdir -p /target/usr/share/polkit-1/actions
mkdir -p /target/usr/share/pixmaps
mkdir -p /target/etc/lightdm
mkdir -p /target/usr/lib/python3/dist-packages
mkdir -p /target/boot/grub

# Copy files to the chroot directory
cp /preseed/grub/grub /target/etc/default
cp /preseed/repos/multimedia.list /target/etc/apt/sources.list.d
cp /preseed/repos/peppermint.list /target/etc/apt/sources.list.d
cp /preseed/repos/sources.list /target/etc/apt
cp /preseed/keyrings/deb-multimedia-keyring.gpg /target/etc/apt/trusted.gpg.d
cp /preseed/keyrings/peppermint-keyring.gpg /target/etc/apt/trusted.gpg.d
cp /preseed/keyrings/deb-multimedia-keyring.gpg /target/usr/share/keyrings
cp /preseed/keyrings/peppermint-keyring.gpg /target/usr/share/keyrings
cp /preseed/apps/* /target/usr/share/applications
cp /preseed/conf/* /target/opt/pepconf
cp /preseed/database/* /target/opt/pypep/dbpep
cp /preseed/polkit/* /target/usr/share/polkit-1/actions
cp /preseed/pixmaps/* /target/usr/share/pixmaps
cp /preseed/lightdm/* /target/etc/lightdm
cp /preseed/autostart/* /target/etc/skel/.config/autostart

# Copy recursive files and sub-directories
cp -r /preseed/grub/themes /target/boot/grub
cp -r /preseed/protools/* /target/usr/local/bin
cp -r /preseed/py/* /target/usr/lib/python3/dist-packages
cp -r /preseed/tools/* /target/etc/skel/.local/share/pmostools

# Run a commands in the chroot
cd /target/usr/share/python-apt/templates/
ln -s Debian.info Peppermint.info
ln -s Debian.mirrors Peppermint.mirrors
cd /target/usr/share/distro-info/
ln -s debian.csv peppermint.csv
cd /target/usr/share/
ln -s icons pepicons
ln -s themes pepthemes
ln -s backgrounds pepwallpaper
chmod 777 pepicons
chmod 777 pepthemes
chmod 777 pepwallpaper

# update gub and initramfs
chroot /target update-initramfs -u
chroot /target update-grub


exit 0


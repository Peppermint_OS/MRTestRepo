# SPDX-License-Identifier: GPL-3.0-or-later
#
# SPDX-FileCopyrightText:  2023 PeppermintOS Team  (peppermintosteam@proton.me)

# This Make file is used to execute the different parts if the bubble system
# by running for example deb64 will chain execute copy_fls, copy_flders and
# inflate to build an ISO . To begin a fresh skeleton, you need to run only
# fresh...after that. As build sare release for publication you can run nightly
# or release to move the files to your web server location the
# run the build you need.
# None of these steps need to run as root all this can be ran ad a normal user

PY_PATH = ~/bubbles/python_modules

## 64 bit ISOs

dev64xfce:
	python3 ${PY_PATH}/build_iso.py set e-xfce b-dev64
deb64xfce:
	python3 ${PY_PATH}/build_iso.py set e-xfce b-deb64
deb64gfb:
	python3 ${PY_PATH}/build_iso.py set e-gnomefb b-deb64
dev64gfb:
	python3 ${PY_PATH}/build_iso.py set e-gnomefb b-dev64
deb64opb:
	python3 ${PY_PATH}/build_iso.py set e-openbox b-deb64
dev64opb:
	python3 ${PY_PATH}/build_iso.py set e-openbox b-dev64

## 32 bit ISOs
deb32xfce:
	python3 ${PY_PATH}/build_iso.py set e-xfce b-deb32
dev32xfce:
	python3 ${PY_PATH}/build_iso.py set e-xfce b-dev32
deb32gfb:
	python3 ${PY_PATH}/build_iso.py set e-gnomefb b-deb32
dev32gfb:
	python3 ${PY_PATH}/build_iso.py set e-gnomefb b-dev32
deb32opb:
	python3 ${PY_PATH}/build_iso.py set e-openbox b-deb32
dev32opb:
	python3 ${PY_PATH}/build_iso.py set e-openbox b-dev32

## arm ISOs
debarmxfce:
	python3 ${PY_PATH}/build_iso.py set e-xfce b-debarm
devarmxfce:
	python3 ${PY_PATH}/build_iso.py set e-xfce b-devarm
debarmgfb:
	python3 ${PY_PATH}/build_iso.py set e-gnomefb b-debarm
devarmgfb:
	python3 ${PY_PATH}/build_iso.py set e-gnomefb b-devarm
debarmopb:
	python3 ${PY_PATH}/build_iso.py set e-openbox b-debarm
devarmopb:
	python3 ${PY_PATH}/build_iso.py set e-openbox b-devarm

## loaded isos
deb64loadxf:
	python3 ${PY_PATH}/build_iso.py set e-loadxf b-deb64
dev64loadxf:
	python3 ${PY_PATH}/build_iso.py set e-loadxf b-dev64
deb32loadxf:
	python3 ${PY_PATH}/build_iso.py set e-loadxf b-deb32
dev32loadxf:
	python3 ${PY_PATH}/build_iso.py set e-loadxf b-dev32
deb64loadgfb:
	python3 ${PY_PATH}/build_iso.py set e-loadgfb b-deb64
dev64loadgfb:
	python3 ${PY_PATH}/build_iso.py set e-loadgfb b-dev64
deb32loadgfb:
	python3 ${PY_PATH}/build_iso.py set e-loadgfb b-deb32
dev32loadgfb:
	python3 ${PY_PATH}/build_iso.py set e-loadgfb b-dev32
deb64loadcin:
	python3 ${PY_PATH}/build_iso.py set e-loadcin b-deb64
dev64loadcin:
	python3 ${PY_PATH}/build_iso.py set e-loadcin b-dev64
deb32loadcin:
	python3 ${PY_PATH}/build_iso.py set e-loadcin b-deb32
dev32loadcin:
	python3 ${PY_PATH}/build_iso.py set e-loadcin b-dev32

## server isos
debserver:
	python3 ${PY_PATH}/build_iso.py set e-server b-deb64
devserver:
	python3 ${PY_PATH}/build_iso.py set e-server b-dev64

## Mini isos
deb64mini:
	python3 ${PY_PATH}/build_iso.py set e-mini b-deb64
dev64mini:
	python3 ${PY_PATH}/build_iso.py set e-mini b-dev64
deb32mini:
	python3 ${PY_PATH}/build_iso.py set e-mini b-deb32
dev32mini:
	python3 ${PY_PATH}/build_iso.py set e-mini b-dev32

release:
	python3 ${PY_PATH}/release.py

# run this to start with a blank shell for
# config folders/
fresh:
	python3 ${PY_PATH}/fresh_bubble.py

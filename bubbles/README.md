# Bubbles

Bubbles is an ISO builder for debian based operating systems, written in python.

This application uses [GNU make](https://www.gnu.org/software/make/manual/make.html)
to bring all the modules together as a semi-automated build system.

It does use the debian live build tools to build and create the ISO.

Requirements needed to use Bubbles:
- debian live-build tools
- python3
- gnu make

**Usage** - The Peppermint ISO collection consists of:
 - 32/64 bit
 - ARM 
 - Systemd/NonSystemd 


## Understand the folder structure

Every piece of the ISO broken down in to a modular structure i.e. the folder
structure you see in the **iso_configs**. Some folders are shared common files 
that all ISOs will use other folders are broken down by specific architecture. 
This makes it easier to scale and mainatain as things change over time.

Here is a grouping break-down of the folders

### Shared Folders

- aliases -  for use of custom bash aliases
- application - the location for custom  application .desktop files
- auto start - launches the welcome screen 
- database - a place to puts any custom sqlite database files
- desktop_base - used to put custom, custome base themes
- face - set the login thumbnail 
- font - any fonts that need to be installed 
- hooks - confogurations that are used during the build process.
- id_files - used for build identification 
- icons - put icons here to share them wil all builds.
- issue - version files for the syste, change  to your build name /version
- lightdm - used for the light dm login manager
- menu - default menu settings
- neofetch - neofetch configurations 
- PepProPixMaps - the peptools icons 
- PepProTools - the Peppemrint tools executables
- plymouth - update the plymouth boot screens
- polkit - manage the policy kits settings
- profile - setup the default profile configuration 
- theme - this will apply the themes to all your builds
- user_config - Default live user configuration
- wallpaper - stanard wallpaper location
- xfce - xfce configurations 
- gnome-flashback - gnome flashback configurations
- openbox - open box configurations
- pmosyools - all the peppemrint os tools
- pylibraires - python libraires used for custom pep tools


### Architecture Specific

- info - Mirror and info settings
- multimedia - keys and repo settings
- sources - defaults repo sources list 
- splash - Grub bood splash configurations 
- os-release - OS version and tesgging settings
- calamares_settings - Calamares installer configurations 
- grub - Manage the grub settings pers build. 
Each folder in this area has subfolders broken down by:
- Debian 32/64/arm
- Devuan 32/64/arm

As you have a need to maintain separate configuration files for each build
this is where you place your files.

*For example:*
If you have Calamares Settings configuration files ONLY for the Devuan 32 bit
ISO, then you put those files in the /calamares_settings/dev_32/ directory.

Make any modifications to the fole in the configurations above. 
When you have done that make sure you are in the bubbles directory.
Then in temerinal run for example ```make deb64``` to build the 64 bit iso.
if you want to run other builds you would use the make command  with one of the
following deb32, dev64, dev32, debarm or devarm.

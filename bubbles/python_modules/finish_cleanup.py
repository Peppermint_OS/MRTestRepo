"""
* Author: "PeppermintOS Team(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* Clean up the working folder and move ISO and other files
"""

import os
import shutil
from pathlib import Path
from datetime import date
import time

# Set the home path used regardless the user logged in
BSTRING_ISO_CONFIGS = '~/bubbles/iso_configs'
HOME_FOLDER = str(Path(BSTRING_ISO_CONFIGS).expanduser())
FUSATO_ROOT = '/fusato'
# Set the base destination location for the final ISO
# Default base is /var/www/html/nightly/
BASE_DESTINATION = '/var/www/html/nightly'
# Set the base ISO Name
BASE_NAME = 'PeppermintOS'



def make_check_sum():
    """ Generate the check sum files """
    os.chdir(HOME_FOLDER + FUSATO_ROOT)
    current_working_directory = os.getcwd().replace('/', '/')
    for _, _, files in os.walk(current_working_directory):
        for fiso in files:
            if fiso.endswith('.iso'):
                sha = "sha512sum "
                ext = "-sha512.checksum"
                gen_sum = sha + fiso + ' >' + fiso + ext
                os.system(gen_sum)


def process_iso(base, arch, desktop):
    """
    Process the ISO for a given base, architecture, and desktop.
    """
    current_working_directory = os.listdir(".")
    today = str(date.today())

    for files in current_working_directory:
        if files.endswith('.iso'):
            src_iso = files
            rdes = f"{BASE_NAME}-{base}_{arch}_{desktop.lower()}{today}.iso"
            des_iso =f"{BASE_DESTINATION}/{base.lower()}{arch}/{desktop.lower()}"
            
            os.rename(src_iso, rdes)
            shutil.copy(rdes, des_iso)

        elif files.endswith('.checksum'):
            src_sum = files
            rdes_sum = f"{BASE_NAME}{base.capitalize()}_{arch}_{desktop.lower()}-sha512.checksum"
            des_sum = f"{BASE_DESTINATION}/{base.lower()}{arch}/{desktop.lower()}"
            os.rename(src_sum, rdes_sum)
            shutil.copy(rdes_sum, des_sum)

def check_build_type():
    """ Decided which ISO needs to be moved for nightly """
    # Make sure you are in fustao
    os.chdir(HOME_FOLDER + FUSATO_ROOT)
    directory_path = HOME_FOLDER + FUSATO_ROOT
    file_function_mapping = {
        'DEB.64xfc': ('deb', '64', 'XFCE'),
        'DEB.32xfc': ('deb', '32', 'XFCE'),
        'DEB.armxfc': ('deb', 'arm', 'XFCE'),
        'DEV.64xfc': ('dev', '64', 'XFCE'),
        'DEV.32xfc': ('dev', '32', 'XFCE'),
        'DEV.armxfc': ('dev', 'arm', 'XFCE'),
        'DEB.64gfb': ('deb', '64', 'GFB'),
        'DEB.32gfb': ('deb', '32', 'GFB'),
        'DEV.64gfb': ('dev', '64', 'GFB'),
        'DEV.32gfb': ('dev', '32', 'GFB'),
        'DEB.64opb': ('deb', '64', 'OPB'),
        'DEB.32opb': ('deb', '32', 'OPB'),
        'DEV.64opb': ('dev', '64', 'OPB'),
        'DEV.32opb': ('dev', '32', 'OPB'),
        'DEBLD.64loadedxf': ('deb', '64', 'LOADEDXF'),
        'DEVLD.64loadedxf': ('dev', '64', 'LOADEDXF'),
        'DEBLD.32loadedxf': ('deb', '32', 'LOADEDXF'),
        'DEVLD.32loadedxf': ('dev', '32', 'LOADEDXF'),
        'DEBLD.64loadedgfb': ('deb', '64', 'LOADEDGFB'),
        'DEVLD.64loadedgfb': ('dev', '64', 'LOADEDGFB'),
        'DEBLD.32loadedgfb': ('deb', '32', 'LOADEDGFB'),
        'DEVLD.32loadedgfb': ('dev', '32', 'LOADEDGFB'),
        'DEBLD.64loadedcin': ('deb', '64', 'LOADEDCIN'),
        'DEVLD.64loadedcin': ('dev', '64', 'LOADEDCIN'),
        'DEBLD.32loadedcin': ('deb', '32', 'LOADEDCIN'),
        'DEVLD.32loadedcin': ('dev', '32', 'LOADEDCIN'),
        'DEBSRV.64server': ('deb', '64', 'SERVER'),
        'DEVSRV.64server': ('dev', '64', 'SERVER'),
        'DEBMIN.64mini': ('deb', '64', 'MINI'),
        'DEVMIN.64mini': ('dev', '64', 'MINI'),
        'DEBMIN.32mini': ('deb', '32', 'MINI'),
        'DEVMIN.32mini': ('dev', '32', 'MINI'),

       }


    def check_build():
        """ Check what build is there and call the correct process function """
        for file_name, params in file_function_mapping.items():
            file_path = os.path.join(directory_path, file_name)
            if os.path.exists(file_path):
                print(f"You are building a {params[0]} {params[1]} {params[2]} ISO type")
                process_iso(*params)
            else:
                print(f"File {file_name} you're not building this ISO type!")


    check_build()


def kill_old_iso():
    """ Delete older ISOs"""
    base_path = BASE_DESTINATION
    arch_list = ['/deb32/', '/deb64/', '/debarm/', '/dev32/', '/dev64/', '/devarm/']
    de_list = ['gfb', 'opb', 'xfce', 'loaded', 'server', 'mini']
    for archs in arch_list:
        for desktops in  de_list:
            full_path = []
            full_path.append(base_path + archs + desktops)
            for working_paths in full_path:
                N = 1
                os.chdir(os.path.join(os.getcwd(), working_paths))
                list_of_files = os.listdir()
                current_time = time.time()
                day = 86400
                for fls in list_of_files:
                    file_location = os.path.join(os.getcwd(), fls)
                    file_time = os.stat(file_location).st_mtime
                    if file_time < current_time - day*N:
                        print(f" Delete : {fls}")
                        os.remove(file_location)

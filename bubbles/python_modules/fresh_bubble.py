"""
* Author: "PeppermintOS Team(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* this will build you a blank config structure.
"""
import os
from pathlib import Path

# Set the home path used regardless the user logged in
bstring = '~/bubbles/iso_configs'
home_folder = Path( bstring ).expanduser()

def set_config_structure():
    """ Create the folders used to store the configs to be used in the ISO
        build process, if the folder already exist nothing hapopens.
        Its there to give you a starting point
    """
    # Move to iso_configs
    os.chdir(home_folder)
    
    # you can add additional, folders to the array as needed, you can also
    # remove a folder if you do not deem it necessary to be used
    folders = ["aliases", "xfce", "wallpaper", "user_config", "theme",
                "sources", "icons", "profile", "polkit",
                "plymouth", "osrelease", "multimedia", "menu", "splash",
                "lightdm", "issue", "info", "hooks", "grub", "font",
                "desktop_base", "database", "calamares_settings",
                "application", "PepProPixMaps", "PepProTools", "packages", 
                "loaded-application", "loaded-hooks", "loaded-wallpaper", 
                "loaded-xfce", "id_files", "server-hooks,", "server-installer", "server-firewall", "server-scripts"
                ]
    for f in folders:
        if os.path.exists(f):
            print("You already have a set of config folders!")
            print("If you want to start with a blank set")
            print("Please delete your current config folders")
        else:
            os.makedirs(f)

set_config_structure()

"""
* Author: "PeppermintOS Team(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This config file is the master configs used to build ISOs oer arch and desktop
* As the need to scale the builds arises, you can add more settings to meet the 
* need. 
"""

##########################
# Functions to be ran
##########################
# inflate_bubble sets the configs files
# infra copies the needed configs
shared_setup_cmds = ('inflate_bubble.set_fusato_structure',
                     'inflate_bubble.set_fusato_installer_structure',
                     'inflate_bubble.set_general_shared',
                     'inflate_bubble.set_grub_shared',
                     'inflate_bubble.set_binary_shared',
                     'inflate_bubble.set_lightdm',
                     'infra.shared_folders',
                     'infra.icons_themes',
                     'infra.shared_files',
                     'infra.set_symlinks',
                     'infra.boostrap_shared',
                     'infra.add_web_profile'
                     )

shared_setup_loaded_cmds = ('inflate_bubble.set_fusato_structure',
                            'inflate_bubble.set_fusato_installer_structure',
                            'inflate_bubble.set_general_shared',
                            'inflate_bubble.set_grub_shared',
                            'inflate_bubble.set_binary_shared',
                            'inflate_bubble.set_lightdm',
                            'infra.shared_folders',
                            'infra.icons_themes',
                            'infra.shared_files',
                            'infra.set_symlinks',
                            'infra.boostrap_shared',
                            'infra.add_web_profile'
                            )

shared_setup_server_cmds = ('inflate_bubble.set_fusato_server_installer_structure',
                            'inflate_bubble.set_fusato_server_structure',
                            'inflate_bubble.set_grub_shared',
                            'inflate_bubble.set_binary_shared',
                            'infra.shared_server_files',
                            'infra.boostrap_shared'
                            )

shared_setup_mini_cmds = ('inflate_bubble.set_fusato_mini_installer_structure',
                          'inflate_bubble.set_binary_shared',
                          'infra.mini_shared_installer_files'
                           )

flag_specific_setup_32_cmds = ('inflate_bubble.set_specific_32_packages',)

# Setup Desktop configs 
# Add mor as needed
setup_xfce_cmds = ('inflate_bubble.set_xfce',
                   'infra.xfce_configs',
                   )
setup_gfb_cmds = ('inflate_bubble.set_gfb',
                  'infra.gnome_flahsbak_configs',
                   )
setup_opb_cmds = ('inflate_bubble.set_opb',
                  'infra.open_box_configs',
                   )
setup_loadedxf_cmds = ('inflate_bubble.set_loadedxf',
                       'infra.loaded_xfce_configs', 
                       'infra.loaded_folders',
                       )
setup_loadedgfb_cmds = ('inflate_bubble.set_loadedgfb',
                        'infra.loaded_gfb_configs', 
                        'infra.loaded_folders',
                         )
setup_loadedcin_cmds = ('inflate_bubble.set_loadedcin',
                        'infra.loaded_cinnamon_configs', 
                        'infra.loaded_folders',
                         )
setup_server_cmds = ('inflate_bubble.set_server',
                     'infra.server_configs',
                     )
setup_mini_cmds = ('inflate_bubble.set_mini',)

# Setup grub chroot
setup_chroot_grub_64 = ('inflate_bubble.set_chroot_grub_64',)
setup_chroot_grub_arm = ('inflate_bubble.set_chroot_grub_arm',)
setup_chroot_grub_32 = ('inflate_bubble.set_chroot_grub_32',)
setup_chroot_grub_64ld = ('inflate_bubble.set_chroot_grub_64ld',)
setup_chroot_grub_64srv = ('inflate_bubble.set_chroot_grub_64srv',)
setup_chroot_grub_64min = ('inflate_bubble.set_chroot_grub_64,min',)

# Setup grub binary
setup_binary_grub_64 = ('inflate_bubble.set_binary_64',)
setup_binary_grub_arm = ('inflate_bubble.set_binary_arm',)
setup_binary_grub_32 = ('inflate_bubble.set_binary_32',)

# Setup Firmware
setup_fw_64_32 = ('inflate_bubble.set_firmware',)
setup_fw_arm = ('inflate_bubble.set_firmware_arm',)

# Combined tuples for building
# Add more as the need arises Per Desktop
build64_xfce_build = (shared_setup_cmds + setup_xfce_cmds +
                      setup_chroot_grub_64 + setup_binary_grub_64 +
                      setup_fw_64_32
                      )

build32_xfce_build = (shared_setup_cmds + setup_xfce_cmds +
                      setup_chroot_grub_32 + setup_binary_grub_32 +
                      setup_fw_64_32 + flag_specific_setup_32_cmds
                      )

buildarm_xfce_build = (shared_setup_cmds + setup_xfce_cmds +
                       setup_chroot_grub_arm + setup_binary_grub_arm +
                       setup_fw_arm
                       )
# Gnome_FlashBack
build64_gfb_build = (shared_setup_cmds + setup_gfb_cmds +
                      setup_chroot_grub_64 + setup_binary_grub_64 +
                      setup_fw_64_32
                      )

build32_gfb_build = (shared_setup_cmds + setup_gfb_cmds +
                      setup_chroot_grub_32 + setup_binary_grub_32 +
                      setup_fw_64_32 + flag_specific_setup_32_cmds
                      )

buildarm_gfb_build = (shared_setup_cmds + setup_gfb_cmds +
                       setup_chroot_grub_arm + setup_binary_grub_arm +
                       setup_fw_arm
                       )

#Open_box
build64_opb_build = (shared_setup_cmds + setup_opb_cmds +
                      setup_chroot_grub_64 + setup_binary_grub_64 +
                      setup_fw_64_32
                      )

build32_opb_build = (shared_setup_cmds + setup_opb_cmds +
                      setup_chroot_grub_32 + setup_binary_grub_32 +
                      setup_fw_64_32 + flag_specific_setup_32_cmds
                      )

buildarm_opb_build = (shared_setup_cmds + setup_opb_cmds +
                       setup_chroot_grub_arm + setup_binary_grub_arm +
                       setup_fw_arm
                       )

#Xfce_loaded
build64_loadedxf_build = (shared_setup_loaded_cmds + setup_loadedxf_cmds + 
                          setup_chroot_grub_64 + setup_binary_grub_64 +
                          setup_fw_64_32
                         )
build32_loadedxf_build = (shared_setup_loaded_cmds + setup_loadedxf_cmds + 
                          setup_chroot_grub_32 + setup_binary_grub_32 +
                          setup_fw_64_32
                          )
#Kde_loaded
build64_loadedgfb_build = (shared_setup_loaded_cmds + setup_loadedgfb_cmds + 
                           setup_chroot_grub_64 + setup_binary_grub_64 +
                           setup_fw_64_32
                           )
build32_loadedgfb_build = (shared_setup_loaded_cmds + setup_loadedgfb_cmds + 
                           setup_chroot_grub_32 + setup_binary_grub_32 +
                           setup_fw_64_32
                           )

#Cinnamon_loaded
build64_loadedcin_build = (shared_setup_loaded_cmds + setup_loadedcin_cmds + 
                           setup_chroot_grub_64 + setup_binary_grub_64 +
                           setup_fw_64_32
                           )
build32_loadedcin_build = (shared_setup_loaded_cmds + setup_loadedcin_cmds + 
                           setup_chroot_grub_32 + setup_binary_grub_32 +
                           setup_fw_64_32
                           )

#Server
build64_server_build = (shared_setup_server_cmds + setup_server_cmds +
                         setup_chroot_grub_64 + setup_binary_grub_64 +
                         setup_fw_64_32 
                         )

#Mini
build64_mini_build = (shared_setup_mini_cmds + setup_mini_cmds +
                       setup_binary_grub_64 + setup_fw_64_32
                       )

build32_mini_build = (shared_setup_mini_cmds + setup_mini_cmds +
                       setup_binary_grub_32 + setup_fw_64_32
                       )

### Inflate bubble section
# Packages that are to be installed as needed per Desktop
# Add more Desktops as the need arises
XFCE_LIST = ('xfce4\n'
             'mousepad\n'
             'xfce4-battery-plugin\n'
             'xfce4-clipman-plugin\n'
             'xfce4-power-manager\n'
             'xfce4-taskmanager\n'
             'xfce4-terminal\n'
             'xfce4-screenshooter\n'
             'xfce4-whiskermenu-plugin\n'
             'xfce4-panel-profiles\n'
             'thunar-archive-plugin\n'
             'thunar-volman\n'
             'xarchiver\n'
             'plank\n'
             'mugshot\n'
             'menulibre\n'
             'python3-xapian\n'
             'apt-xapian-index\n'
            )
GNOME_FLASHBACK_LIST = ('alacarte\n'
                        'eog\n'
                        'evince\n'
                        'file-roller\n'
                        'gedit\n'
                        'gnome-calculator\n'
                        'gnome-control-center\n'
                        'gnome-tweaks\n'
                        'gnome-screenshot\n'
                        'gnome-session-flashback\n'
                        'gnome-terminal\n'
                        'nautilus\n'
                        'yelp\n'
                         )
OPENBOX_LIST = ('openbox\n'
                'tint2\n'
                'thunar\n'
                'thunar-archive-plugin\n'
                'mousepad\n'
                'lxpolkit\n'
                'pnmixer\n'
                'gmrun\n'
                'nitrogen\n'
                'compton\n'
                'compton-conf\n'
                'compton-conf-l10n\n'
                'xarchiver\n'
                'lxsession-logout\n'
                'xfce4-screenshooter\n'
                'xfce4-settings\n'
                'xfce4-terminal\n'
                'marwaita-peppermint-gtk-theme\n'
                'tela-icon-theme\n'
                'openbox-theme-collection\n'
                'xdg-user-dirs-gtk\n'
                'gnome-screensaver\n'
                'cbatticon\n'
                'dunst\n'
                'xfce4-clipman\n'
                'xfce4-power-manager\n'
                'plank\n'
                'obmenu-generator\n'
                'qt5-style-plugins \n'
                'qt5ct\n' 
                'jgmenu\n'
                )

LOADED_XFCE_LIST = ('xfce4\n'
                    'xfce4-goodies\n'
                    'apt-config-auto-update\n'
                    'gnome-system-tools\n'
                    'gvfs-backends\n'
                    'blueman\n'
                    'bluez-cups\n'
                    'bluez-alsa-utils\n'
                    'gufw\n'
                    'tela-icon-theme\n'
                    'system-config-printer\n'
                    'menulibre\n'
                    'xscreensaver\n'
                    'xscreensaver-data\n'
                    'xscreensaver-data-extra\n'
                    'xscreensaver-gl\n'
                    'xscreensaver-gl-extra\n'
                    'gnome-calculator\n'
                    'accountsservice\n'
                    'catfish\n'
                    'timeshift\n'
                    'pepinstall\n'
                    'sticky\n'
                    'mintstick\n'
                    'peplocale\n'
                    'bleachbit\n'
                    'gimp\n'
                    'gimp-data-extras\n'
                    'inkscape\n'
                    'atril\n'
                    'transmission-gtk\n'
                    'thunderbird\n'
                    'thunderbird-l10n-all\n'
                    'libreoffice\n'
                    'libreoffice-gtk3\n'
                    'libreoffice-l10n*\n'
                    'printer-driver-cups-pdf\n'
                    'gnome-2048\n'
                    'gnome-chess\n'
                    'gnome-mahjongg\n'
                    'gnome-sudoku\n'
                    'cheese\n'
                    'parole \n'
                    'qt5-style-plugins\n'
                    'qt5ct\n'
                    'xfsdump\n'
                    'tumbler\n'
                    'tumbler-plugins-extra\n'
                    'ffmpegthumbnailer\n'
                    'baobab\n'
                    'xdg-user-dirs-gtk\n'
                    'flatpak\n'
                    'gir1.2-flatpak-1.0\n'
                    'ntpsec-ntpdate\n'
                    'ntpsec\n'
                    'mugshot\n'
                    )

LOADED_GFB_LIST = ('alacarte\n'
                   'eog\n'
                   'file-roller\n'
                   'gnome-control-center\n'
                   'gnome-tweaks\n'
                   'gnome-session-flashback\n'
                   'nautilus\n'
                   'yelp\n'
                   'gnome-screenshot\n'
                   'gnome-terminal\n'
                   'gedit\n'
                   'gedit-plugins\n'
                   'eog\n'
                   'numlockx\n'
                   'blueman\n'
                   'bluez-cups\n'
                   'bluez-alsa-utils\n'
                   'gufw\n'
                   'apt-config-auto-update\n'
                   'gnome-system-tools\n'
                   'gvfs-backends\n'
                   'tela-icon-theme\n'
                   'system-config-printer\n'
                   'gnome-screensaver\n'
                   'gnome-calculator\n'
                   'accountsservice\n'
                   'timeshift\n'
                   'pepinstall\n'
                   'sticky\n'
                   'mintstick\n'
                   'peplocale\n'
                   'bleachbit\n'
                   'gimp\n'
                   'gimp-data-extras\n'
                   'inkscape\n'
                   'atril\n'
                   'transmission-gtk\n'
                   'thunderbird\n'
                   'thunderbird-l10n-all\n'
                   'libreoffice\n'
                   'libreoffice-gtk3\n'
                   'libreoffice-l10n*\n'
                   'printer-driver-cups-pdf\n'
                   'gnome-2048\n'
                   'gnome-chess\n'
                   'gnome-mahjongg\n'
                   'gnome-sudoku\n'
                   'cheese\n'
                   'parole \n'
                   'qt5-style-plugins\n'
                   'qt5ct\n'
                   'xfsdump\n'
                   'ffmpegthumbnailer\n'
                   'baobab\n'
                   'xdg-user-dirs-gtk\n'
                   'flatpak\n'
                   'gir1.2-flatpak-1.0\n'
                   'ntpsec-ntpdate\n'
                   'ntpsec\n'
                        )

LOADED_CINNAMON_LIST = ('cinnamon\n'
                        'nemo gir1.2-nemo-3.0\n'
                        'nemo-fileroller \n'
                        'nemo-gtkhash\n'
                        'muffin\n'
                        'gnome-screenshot\n'
                        'gnome-terminal\n'
                        'gedit\n'
                        'gedit-plugins\n'
                        'eog\n'
                        'numlockx\n'
                        'blueman\n'
                        'bluez-cups\n'
                        'bluez-alsa-utils\n'
                        'gufw\n'
                        'apt-config-auto-update\n'
                        'gnome-system-tools\n'
                        'gvfs-backends\n'
                        'tela-icon-theme\n'
                        'system-config-printer\n'
                        'xscreensaver\n'
                        'xscreensaver-data\n'
                        'xscreensaver-data-extra\n'
                        'xscreensaver-gl\n'
                        'xscreensaver-gl-extra\n'
                        'gnome-calculator\n'
                        'accountsservice\n'
                        'catfish\n'
                        'timeshift\n'
                        'pepinstall\n'
                        'sticky\n'
                        'mintstick\n'
                        'peplocale\n'
                        'bleachbit\n'
                        'gimp\n'
                        'gimp-data-extras\n'
                        'inkscape\n'
                        'atril\n'
                        'transmission-gtk\n'
                        'thunderbird\n'
                        'thunderbird-l10n-all\n'
                        'libreoffice\n'
                        'libreoffice-gtk3\n'
                        'libreoffice-l10n*\n'
                        'printer-driver-cups-pdf\n'
                        'gnome-2048\n'
                        'gnome-chess\n'
                        'gnome-mahjongg\n'
                        'gnome-sudoku\n'
                        'cheese\n'
                        'parole \n'
                        'qt5-style-plugins\n'
                        'qt5ct\n'
                        'xfsdump\n'
                        'ffmpegthumbnailer\n'
                        'baobab\n'
                        'xdg-user-dirs-gtk\n'
                        'flatpak\n'
                        'gir1.2-flatpak-1.0\n'
                        'ntpsec-ntpdate\n'
                        'ntpsec\n'
                        )

SERVER_LIST = ('zonefstoolspep\n'
               'dmzonedtoolspep\n'
               'libzbdpep1\n'
               'sudo\n'
               'task-ssh-server\n'
               'task-web-server\n'
               'sshguard\n'
               'btop\n'
               'whois\n'
               'rkhunter\n'
               'debsecan\n'
               'net-tools\n'
               'nfs-common\n'
               'firewalld\n'
               'samba\n'
               'cups\n'
               'gvfs-backends\n'
               'git\n'
               'wget\n'
               )

MINI_LIST = ('nano\n'
               )

# The Light DM Login Settings
LIGHT_DM_LIST = ('lightdm\n'
                 'lightdm-gtk-greeter\n'
                 'lightdm-gtk-greeter-settings\n'
                )

# General Shared packages to be installed. 
GENERAL_SHARED_LIST = ('alsa-utils\n'
                       'xorg\n'
                       'xserver-xorg\n'
                       'xserver-xorg-input-synaptics\n'
                       'xserver-xorg-input-all\n'
                       'xserver-xorg-video-vmware\n'
                       'xserver-xorg-video-all\n'
                       'bluez\n'
                       'btop\n'
                       'calamares\n'
                       'calamares-settings-debian\n'
                       'console-setup\n'
                       'cups\n'
                       'curl\n'
                       'dconf-editor\n'
                       'dkms\n'
                       'dbus-x11\n'
                       'desktop-base\n'
                       'fonts-cantarell\n'
                       'fonts-liberation\n'
                       'f2fs-tools\n'
                       'gdebi\n'
                       'gir1.2-webkit2-4.0\n'
                       'git\n'
                       'gnome-disk-utility\n'
                       'gnome-system-tools\n'
                       'gparted\n'
                       'gvfs-backends\n'
                       'inputattach\n'
                       'inxi\n'
                       'locales\n'
                       'libgtk2.0-0\n'
                       'libgtk2.0-common\n'
                       'nala\n'
                       'network-manager-gnome\n'
                       'ntp\n'
                       'pulseaudio-module-bluetooth\n'
                       'python3-pip\n'
                       'python3-tk\n'
                       'python3-pil.imagetk\n'
                       'synaptic\n'
                       'system-config-printer\n'
                       'simple-scan\n'
                       'smartmontools\n'
                       'smbclient\n'
                       'spice-vdagent\n'
                       'sqlite3\n'
                       'wireless-tools\n'
                       'wget\n'
                       'xfsprogs\n'
                       'plank\n'
                       )

# Grub things shared by all 
GRUB_LIST_SHARED = ('efibootmgr\n'
                    'grub-common\n'
                    'grub2-common\n'
                    'grub-efi\n'
                    'libefiboot1\n'
                    'libefivar1\n'
                    'mokutil\n'
                    'os-prober\n'
                    'shim-signed\n'
                    'shim-signed-common\n'
                    'shim-unsigned\n'
                    'libglib2.0\n'
                    )

# Grub things shared by 64bit
GRUB_LIST_64 = ('grub-efi-amd64\n'
                'grub-efi-amd64-bin\n'
                'grub-efi-amd64-signed\n'
                'shim-helpers-amd64-signed\n'
                'grub-efi-ia32-bin\n'
                )

# Grub things shared by 64bit
GRUB_LIST_64LD = ('grub-efi-amd64\n'
                'grub-efi-amd64-bin\n'
                'grub-efi-amd64-signed\n'
                'shim-helpers-amd64-signed\n'
                'grub-efi-ia32-bin\n'
                )

# Grub things shared by ARM
GRUB_LIST_ARM64 = ('grub-efi-arm64\n'
                   'grub-efi-arm64-bin\n'
                   'grub-efi-arm64-signed\n'
                   'shim-helpers-arm64-signed\n'
                   )

# Grub things shared by 32bit
GRUB_LIST_32 = ('efibootmgr\n'
                'grub-common\n'
                'grub2-common\n'
                'grub-efi\n'
                'grub-efi-ia32\n'
                'grub-efi-ia32-bin\n'
                'grub-efi-ia32-signed\n'
                'grub-efi-ia32-bin\n'
                'os-prober\n'
                'shim-helpers-i386-signed\n'
                )

# Packages for ONLY 32bit builds for the flagship
FLAG_SPECIFIC_LIST_32=('luakit\n')


# Firmware packages shared by 32 adn 64 bit
FIRMWARE_LIST_32_64 = ('atmel-firmware\n'
                       'bluez-firmware\n'
                       'firmware-atheros\n'
                       'firmware-amd-graphics\n'
                       'firmware-bnx2\n'
                       'firmware-bnx2x\n'
                       'firmware-brcm80211\n'
                       'firmware-cavium\n'
                       'firmware-intel-sound\n'
                       'firmware-iwlwifi\n'
                       'firmware-libertas\n'
                       'firmware-linux\n'
                       'firmware-linux-free\n'
                       'firmware-linux-nonfree\n'
                       'firmware-misc-nonfree\n'
                       'firmware-myricom\n'
                       'firmware-netronome\n'
                       'firmware-netxen\n'
                       'firmware-qcom-media\n'
                       'firmware-qcom-soc\n'
                       'firmware-qlogic\n'
                       'firmware-realtek\n'
                       'firmware-samsung\n'
                       'firmware-siano\n'
                       'firmware-sof-signed\n'
                       'midisport-firmware\n'
                       )

# Frimware packages used by ARM
FIRMWARE_LIST_ARM = ('firmware-atheros\n'
                     'firmware-bnx2\n'
                     'firmware-bnx2x\n'
                     'firmware-brcm80211\n'
                     'firmware-iwlwifi\n'
                     'firmware-linux\n'
                     'firmware-linux-nonfree\n'
                     'firmware-libertas\n'
                     'firmware-misc-nonfree\n'
                     'firmware-netxen\n'
                     'firmware-realtek\n'
                     'firmware-ralink\n'
                     'firmware-zd1211\n'
                     )

# Binary packages shared by all
BINARY_LIST_SHARED = ('efibootmgr\n'
                      'grub-common\n'
                      'grub2-common\n'
                      'grub-efi\n'
                      'libefiboot1\n'
                      'libefivar1\n'
                      'mokutil\n'
                      'os-prober\n'
                      'shim-signed\n'
                      'shim-signed-common\n'
                      'shim-unsigned\n'
                      )

# Binary packages shared by 64bit
BINARY_LIST_64 = ('grub-efi-amd64\n'
                  'grub-efi-amd64-bin\n'
                  'grub-efi-amd64-signed\n'
                  'shim-helpers-amd64-signed\n'
                  'grub-efi-ia32-bin\n'
                  )

# Binary packages shared by 64bit
BINARY_LIST_64SRV = ('grub-efi-amd64\n'
                     'grub-efi-amd64-bin\n'
                     'grub-efi-amd64-signed\n'
                     'shim-helpers-amd64-signed\n'
                     'grub-efi-ia32-bin\n'
                  )

# Binary packages shared by ARM
BINARY_LIST_ARM = ('grub-efi-arm64\n'
                   'grub-efi-arm64-bin\n'
                   'grub-efi-arm64-signed\n'
                   'shim-helpers-arm64-signed\n'
                   )

# Binary packages shared by 32bit
BINARY_LIST_32 = ('grub-efi-ia32\n'
                  'grub-efi-ia32-bin\n'
                  'grub-efi-ia32-signed\n'
                  'grub-efi-ia32-bin\n'
                  'shim-helpers-i386-signed\n'
                  )

### END Inflate

# Set the LB  configs
# Shared by all
LBSET_MAIN_SHARED = ('lb config noauto'
                     ' --archive-areas "main contrib non-free non-free-firmware"'
                     ' --apt-recommends true --backports false'
                     ' --binary-images iso-hybrid --cache true'
                     ' --checksums sha512 --clean --color'
                     ' --firmware-binary true --firmware-chroot false'
                     ' --iso-application "PeppermintOS"'
                     ' --iso-preparer "PeppermintOS-https://peppermintos.com/"'
                     ' --iso-publisher "Peppermint OS Team"'
                     ' --iso-volume "PeppermintOS" --mode debian --quiet'
                     ' --security true --zsync false'
                     ' --updates true --win32-loader false'
                     )

# Shared by architecture
LBSET_SHARED_32 = (' --architectures i386 --linux-flavours 686-pae'
                   ' --uefi-secure-boot enable')
LBSET_SHARED_64 = (' --architectures amd64 --linux-flavours amd64')
LBSET_SHARED_ARM = (' --architectures arm64 --linux-flavours arm64'
                    ' --bootstrap-qemu-arch arm64 --bootloaders grub-efi'
                    ' --bootstrap-qemu-static /usr/sbin/qemu-debootstrap'
                    )

# Shared By Debian
LBSET_DEBIAN_SHARED = (
' --distribution bookworm'
' --mirror-bootstrap https://deb.debian.org/debian'
' --parent-mirror-bootstrap https://deb.debian.org/debian'
' --parent-mirror-chroot https://deb.debian.org/debian'
' --parent-mirror-chroot-security https://security.debian.org/debian-security'
' --parent-mirror-binary https://deb.debian.org/debian'
' --parent-mirror-binary-security https://security.debian.org/debian-security'
' --mirror-chroot https://deb.debian.org/debian'
' --mirror-chroot-security https://security.debian.org/debian-security'
                       )

# Shared By Devuan
LBSET_DEVUAN_SHARED = (
' --distribution daedalus'
' --initsystem sysvinit'
' --mirror-bootstrap http://deb.devuan.org/merged'
' --parent-mirror-bootstrap http://deb.devuan.org/merged'
' --parent-mirror-chroot http://deb.devuan.org/merged'
' --parent-mirror-chroot-security http://deb.devuan.org/merged'
' --parent-mirror-binary http://deb.devuan.org/merged'
' --parent-mirror-binary-security http://deb.devuan.org/merged'
' --mirror-chroot http://deb.devuan.org/merged'
' --mirror-chroot-security http://deb.devuan.org/merged'
)

# Private by ISO build not shared
LBSET_PRIVATE_DEB32 = (' --image-name "PeppermintOS-Debian-32"')
LBSET_PRIVATE_DEB64 = (' --image-name "PeppermintOS-Debian-64"')
LBSET_PRIVATE_DEV32 = (' --image-name "PeppermintOS-Devuan-32"')
LBSET_PRIVATE_DEV64 = (' --image-name "PeppermintOS-Devuan-64"')
LBSET_PRIVATE_DEBARM = (' --image-name "PeppermintOS-Debian-ARM"')
LBSET_PRIVATE_DEVARM = (' --debootstrap-options "--merged-usr"'
                        ' --image-name "PeppermintOS-Devuan-ARM"')
LBSET_PRIVATE_LOADED_DEB64 = (' --image-name "PeppermintOS-Loaded-Debian-64"')
LBSET_PRIVATE_LOADED_DEV64 = (' --image-name "PeppermintOS-Loaded-Devuan-64"')
LBSET_PRIVATE_LOADED_DEB32 = (' --image-name "PeppermintOS-Loaded-Debian-32"')
LBSET_PRIVATE_LOADED_DEV32 = (' --image-name "PeppermintOS-Loaded-Devuan-32"')

# Private to de server and mini builds
LBSET_SHARED_INSTALLER_DEB = (' --debian-installer-distribution "bookworm"'
                              ' --debian-installer-gui true'
                              )
LBSET_SHARED_INSTALLER_DEV = (' --debian-installer-distribution "daedalus"'
                              ' --debian-installer-gui true'
                              ' --parent-mirror-debian-installer http://deb.devuan.org/devuan'
                              )
LBSET_SHARED_INSTALLER_MINI = (' --debootstrap-options --include=zstd,locales,dialog,krb5-locales'
                               ' --debian-installer cdrom'
                               )
LBSET_PRIVATE_SERVER_DEB64 = (' --image-name "PeppermintOS-server-Debian-64"'
                              ' --debian-installer live'
                              )
LBSET_PRIVATE_SERVER_DEV64 = (' --image-name "PeppermintOS-server-Devuan-64"'
                              ' --debian-installer live'
                              )
LBSET_PRIVATE_MINI_DEB64 = (' --image-name "PeppermintOS-mini-Debian-64"')
LBSET_PRIVATE_MINI_DEV64 = (' --image-name "PeppermintOS-mini-Devuan-64"')
LBSET_PRIVATE_MINI_DEB32 = (' --image-name "PeppermintOS-mini-Debian-32"')
LBSET_PRIVATE_MINI_DEV32 = (' --image-name "PeppermintOS-mini-Devuan-32"')

# These are the correct order combined LB commands for the builds by ISO name
# these are used during the pipeline building process. 
LBSET_DEB32 = (LBSET_MAIN_SHARED + LBSET_SHARED_32
               + LBSET_DEBIAN_SHARED + LBSET_PRIVATE_DEB32
               )
LBSET_DEB64 = (LBSET_MAIN_SHARED + LBSET_SHARED_64 + LBSET_DEBIAN_SHARED
               + LBSET_PRIVATE_DEB64
               )
LBSET_DEV32 = (LBSET_MAIN_SHARED + LBSET_SHARED_32 + LBSET_DEVUAN_SHARED
               + LBSET_PRIVATE_DEV32
               )
LBSET_DEV64 = (LBSET_MAIN_SHARED + LBSET_SHARED_64 + LBSET_DEVUAN_SHARED
               + LBSET_PRIVATE_DEV64
               )
LBSET_DEVarm =(LBSET_MAIN_SHARED + LBSET_SHARED_ARM + LBSET_DEVUAN_SHARED
               + LBSET_PRIVATE_DEVARM
               )
LBSET_DEBarm =(LBSET_MAIN_SHARED + LBSET_SHARED_ARM + LBSET_DEBIAN_SHARED
               + LBSET_PRIVATE_DEBARM
               )
LBSET_DEBLD64 = (LBSET_MAIN_SHARED + LBSET_SHARED_64 + LBSET_DEBIAN_SHARED
                 + LBSET_PRIVATE_LOADED_DEB64
                 )
LBSET_DEVLD64 = (LBSET_MAIN_SHARED + LBSET_SHARED_64 + LBSET_DEVUAN_SHARED
                 + LBSET_PRIVATE_LOADED_DEV64
                 )
LBSET_DEBLD32 = (LBSET_MAIN_SHARED + LBSET_SHARED_32 + LBSET_DEBIAN_SHARED
                 + LBSET_PRIVATE_LOADED_DEB32
                 )
LBSET_DEVLD32 = (LBSET_MAIN_SHARED + LBSET_SHARED_32 + LBSET_DEVUAN_SHARED
                 + LBSET_PRIVATE_LOADED_DEV32
                 )
LBSET_DEBSRV64 = (LBSET_MAIN_SHARED + LBSET_SHARED_64 + LBSET_DEBIAN_SHARED
                  + LBSET_PRIVATE_SERVER_DEB64 + LBSET_SHARED_INSTALLER_DEB
                  )
LBSET_DEVSRV64 = (LBSET_MAIN_SHARED + LBSET_SHARED_64 + LBSET_DEVUAN_SHARED
                  + LBSET_PRIVATE_SERVER_DEV64 + LBSET_SHARED_INSTALLER_DEV
                  )
LBSET_DEBMIN64 = (LBSET_MAIN_SHARED + LBSET_SHARED_64 + LBSET_DEBIAN_SHARED
                  + LBSET_PRIVATE_MINI_DEB64 + LBSET_SHARED_INSTALLER_DEB
                  + LBSET_SHARED_INSTALLER_MINI
                  )
LBSET_DEVMIN64 = (LBSET_MAIN_SHARED + LBSET_SHARED_64 + LBSET_DEVUAN_SHARED
                  + LBSET_PRIVATE_MINI_DEB64 + LBSET_SHARED_INSTALLER_DEV
                  + LBSET_SHARED_INSTALLER_MINI
                  )
LBSET_DEBMIN32 = (LBSET_MAIN_SHARED + LBSET_SHARED_32 + LBSET_DEBIAN_SHARED
                  + LBSET_PRIVATE_MINI_DEB32 + LBSET_SHARED_INSTALLER_DEB
                  + LBSET_SHARED_INSTALLER_MINI
                  )
LBSET_DEVMIN32 = (LBSET_MAIN_SHARED + LBSET_SHARED_32 + LBSET_DEVUAN_SHARED
                  + LBSET_PRIVATE_MINI_DEB32 + LBSET_SHARED_INSTALLER_DEV
                  + LBSET_SHARED_INSTALLER_MINI
                  )

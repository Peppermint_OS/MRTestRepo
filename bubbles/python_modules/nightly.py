"""
* Author: "PeppermintOS Team(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* Publish Nightly ISO
"""
import finish_cleanup

copy_iso_file_nightly()

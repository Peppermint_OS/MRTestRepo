"""
* Author: "PeppermintOS Team(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* Build the ISOs for the system to be deployed
"""
import os
from pathlib import Path
import argparse
import collections
import logging
import conf
import infra
import inflate_bubble
import finish_cleanup

# Set the home path used regardless the user logged in
BSTRING_ISO_CONFIGS = '~/bubbles/iso_configs'
HOME_FOLDER = str(Path(BSTRING_ISO_CONFIGS).expanduser())
FUSATO_ROOT = '/fusato'
WPCHROOT = '/fusato/config/includes.chroot'
WPINSTALLER = '/fusato/config/includes.installer'


# Set up the logging format
logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

# Common logger Messages
WRKING_DIR = "Current working directory"
CFG_TREE_READY = "Config tree ready!"
CFG_READY = "Configs in place start the ISO build process"
START_LIVEBUILD = "Start Live-Build Process"
BUILD_COMPILED = "ISO build has compiled successfully"
BUILD_COMPLETED = "ISO is ready for usage. Begin copy to Nightly for QA testing"

class BuildBase:
    """
    Base class for building ISOs.
    """
    def __init__(self, sbase, sarch, desktop_helper):
        """ init the building process"""
        self.sbase = sbase
        self.sarch = sarch
        self.change_to_fusato_root_and_build(desktop_helper)

    def change_to_fusato_root_and_build(self, desktop_helper):
        """ Change to the fusato root and build the ISO """
        os.chdir(HOME_FOLDER + FUSATO_ROOT)
        self.build_system(desktop_helper)

    def lb_helper(self):
        """ Helper to set  the lb commands for live-build"""
        lbsetup = None
        if self.sbase in ["deb", "dev", "debmin", "devmin", "debsrv", "devsrv", "debld", "devld"]:
            arch_suffix = self.sarch[1:]
            if arch_suffix in ["64", "32", "arm"]:
                lbsetup = getattr(
                            conf, f"LBSET_{self.sbase.upper()}{arch_suffix}"
                            )
                os.system(lbsetup)
        return lbsetup

    def infra_helper(self):
        """ Get the build infrastucture ready """
        infra_methods = [infra.BinaryFolders, infra.ChrootInstallerFiles,
                         infra.ArchitectureFiles, infra.Archive,
                         infra.InstallerFiles, infra.FusatoConfigs,
                         ]
        for method in infra_methods:
            method(self.sbase, self.sarch)

    def cleanup_helper(self):
        """ Clean and Move the comleted ISO """
        finish_cleanup.make_check_sum()
        finish_cleanup.check_build_type()
        finish_cleanup.kill_old_iso()

    def build_system(self, desktop_helper):
        """ The actual building process """
        current_working_directory = os.getcwd()
        dir_current = WRKING_DIR + current_working_directory
        logger.info(f"Building a {self.sbase}{self.sarch[1:]} bit ISO")
        logger.info(dir_current)
        self.lb_helper()
        desktop_helper()
        self.infra_helper()
        logger.info(CFG_READY)
        logger.info(START_LIVEBUILD)
        logger.info(dir_current)
        self.lb_helper()
        run_cmd = 'sudo lb build'
        os.chdir(HOME_FOLDER + FUSATO_ROOT)
        desktop_name = desktop_helper.__name__.split('_')[-1]
        if desktop_name == 'xfce':
            inflate_bubble.make_bld_xfce(self.sbase, self.sarch[1:])
        elif desktop_name == 'gfb':
            inflate_bubble.make_bld_gnomeflashback(self.sbase, self.sarch[1:])
        logger.info(dir_current)
        os.system(run_cmd)
        logger.info(BUILD_COMPLETED)
        logger.info(dir_current)
        self.cleanup_helper()
        logger.info(BUILD_COMPLETED)


class BuildXfce(BuildBase):
    """ This class will ensure the xfce builds are built """
    def __init__(self, sbase, sarch):
        """ 
            init the building, super will 
            ensure that the method resolution order (MRO) is followed 
            correctly, allowing for a smooth inheritance hierarchy.
        """
        super().__init__(sbase, sarch, self.xfce_helper)

    def xfce_helper(self):
        """ The helper functions to ensure the xfce requirements are used """
        arch_suffix = self.sarch[1:]
        cmd_list = None
        if self.sbase in ["deb", "dev", "debmin", "devmin", "debsrv",
                          "devsrv", "debld", "devld"]:
            if arch_suffix in ["64", "32", "arm"]:
                os.system(f'touch {self.sbase.upper()}.{arch_suffix}xfc')
        if arch_suffix in ["64", "32", "arm"]:
            cmd_list = getattr(conf, f"build{arch_suffix}_xfce_build", [])
            for command in cmd_list:
                execute = command + '()'
                exec(execute)
        return cmd_list


class BuildGflashback(BuildBase):
    """ This class will ensure the gnomeflashback builds are built """
    def __init__(self, sbase, sarch):
        """ 
            init the building, super will
            ensure that the method resolution order (MRO) is followed
            correctly, allowing for a smooth inheritance hierarchy
        """
        super().__init__(sbase, sarch, self.gfb_helper)

    def gfb_helper(self):
        """
        The helper functions to ensure the gnome flashback requirements
        are used
        """
        arch_suffix = self.sarch[1:]
        cmd_list = None
        if self.sbase in ["deb", "dev", "debmin", "devmin", "debsrv", "devsrv", "debld", "devld"]:
            if arch_suffix in ["64", "32", "arm"]:
                os.system(f'touch {self.sbase.upper()}.{arch_suffix}gfb')
        if arch_suffix in ["64", "32", "arm"]:
            cmd_list = getattr(conf, f"build{arch_suffix}_gfb_build", [])
            for command in cmd_list:
                execute = command + '()'
                exec(execute)
        return cmd_list


class BuildOpenbox(BuildBase):
    """ This class will ensure the openbox builds are built """
    def __init__(self, sbase, sarch):
        """ 
            init the building, super will
            ensure that the method resolution order (MRO) is followed
            correctly, allowing for a smooth inheritance hierarchy
        """
        super().__init__(sbase, sarch, self.opb_helper)

    def opb_helper(self):
        """
        The helper functions to ensure the openbox requirements are 
        used
        """
        arch_suffix = self.sarch[1:]
        cmd_list = None
        if self.sbase in ["deb", "dev", "debmin", "devmin", "debsrv", "devsrv", "debld", "devld"]:
            if arch_suffix in ["64", "32", "arm"]:
                os.system(f'touch {self.sbase.upper()}.{arch_suffix}opb')
        if arch_suffix in ["64", "32", "arm"]:
            cmd_list = getattr(conf, f"build{arch_suffix}_opb_build", [])
            for command in cmd_list:
                execute = command + '()'
                exec(execute)
        return cmd_list


class BuildLoadedxf(BuildBase):
    """ This class will ensure the loaded xfce builds are built """
    def __init__(self, sbase, sarch):
        """ 
            init the building, super will
            ensure that the method resolution order (MRO) is followed
            correctly, allowing for a smooth inheritance hierarchy
        """
        super().__init__(sbase, sarch, self.loadedxf_helper)

    def loadedxf_helper(self):
        """
        The helper functions to ensure the loaded xfce requirements are 
        used
        """
        arch_suffix = self.sarch[1:]
        cmd_list = None
        if self.sbase in ["deb", "dev", "debmin", "devmin", "debsrv", "devsrv", "debld", "devld"]:
            if arch_suffix in ["64", "32", "arm"]:
                os.system(f'touch {self.sbase.upper()}.{arch_suffix}loadedxf')
        if arch_suffix in ["64", "32", "arm"]:
            cmd_list = getattr(conf, f"build{arch_suffix}_loadedxf_build", [])
            for command in cmd_list:
                execute = command + '()'
                exec(execute)
        return cmd_list

class BuildLoadedgfb(BuildBase):
    """ This class will ensure the loaded kde builds are built """
    def __init__(self, sbase, sarch):
        """ 
            init the building, super will
            ensure that the method resolution order (MRO) is followed
            correctly, allowing for a smooth inheritance hierarchy
        """
        super().__init__(sbase, sarch, self.loadedgfb_helper)

    def loadedgfb_helper(self):
        """
        The helper functions to ensure the loaded kde requirements are 
        used
        """
        arch_suffix = self.sarch[1:]
        cmd_list = None
        if self.sbase in ["deb", "dev", "debmin", "devmin", "debsrv", "devsrv", "debld", "devld"]:
            if arch_suffix in ["64", "32", "arm"]:
                os.system(f'touch {self.sbase.upper()}.{arch_suffix}loadedgfb')
        if arch_suffix in ["64", "32", "arm"]:
            cmd_list = getattr(conf, f"build{arch_suffix}_loadedgfb_build", [])
            for command in cmd_list:
                execute = command + '()'
                exec(execute)
        return cmd_list

class BuildLoadedcin(BuildBase):
    """ This class will ensure the loaded cinnamon builds are built """
    def __init__(self, sbase, sarch):
        """ 
            init the building, super will
            ensure that the method resolution order (MRO) is followed
            correctly, allowing for a smooth inheritance hierarchy
        """
        super().__init__(sbase, sarch, self.loadedcin_helper)

    def loadedcin_helper(self):
        """
        The helper functions to ensure the loaded cinnamon requirements are 
        used
        """
        arch_suffix = self.sarch[1:]
        cmd_list = None
        if self.sbase in ["deb", "dev", "debmin", "devmin", "debsrv", "devsrv", "debld", "devld"]:
            if arch_suffix in ["64", "32", "arm"]:
                os.system(f'touch {self.sbase.upper()}.{arch_suffix}loadedcin')
        if arch_suffix in ["64", "32", "arm"]:
            cmd_list = getattr(conf, f"build{arch_suffix}_loadedcin_build", [])
            for command in cmd_list:
                execute = command + '()'
                exec(execute)
        return cmd_list

class BuildServer(BuildBase):
    """ This class will ensure the Server builds are built """
    def __init__(self, sbase, sarch):
        """ 
            init the building, super will
            ensure that the method resolution order (MRO) is followed
            correctly, allowing for a smooth inheritance hierarchy
        """
        super().__init__(sbase, sarch, self.server_helper)

    def server_helper(self):
        """
        The helper functions to ensure the server requirements are 
        used
        """
        arch_suffix = self.sarch[1:]
        cmd_list = None
        if self.sbase in ["deb", "dev", "debmin", "devmin", "debsrv", "devsrv", "debld", "devld"]:
            if arch_suffix in ["64", "32", "arm"]:
                os.system(f'touch {self.sbase.upper()}.{arch_suffix}server')
        if arch_suffix in ["64", "32", "arm"]:
            cmd_list = getattr(conf, f"build{arch_suffix}_server_build", [])
            for command in cmd_list:
                execute = command + '()'
                exec(execute)
        return cmd_list


class BuildMini(BuildBase):
    """ This class will ensure the Mini builds are built """
    def __init__(self, sbase, sarch):
        """ 
            init the building, super will
            ensure that the method resolution order (MRO) is followed
            correctly, allowing for a smooth inheritance hierarchy
        """
        super().__init__(sbase, sarch, self.mini_helper)

    def mini_helper(self):
        """
        The helper functions to ensure the Mini requirements are 
        used
        """
        arch_suffix = self.sarch[1:]
        cmd_list = None
        if self.sbase in ["deb", "dev", "debmin", "devmin", "debsrv",
        "devsrv", "debld", "devld"
        ]:
            if arch_suffix in ["64", "32", "arm"]:
                os.system(f'touch {self.sbase.upper()}.{arch_suffix}mini')
        if arch_suffix in ["64", "32", "arm"]:
            cmd_list = getattr(conf, f"build{arch_suffix}_mini_build", [])
            for command in cmd_list:
                execute = command + '()'
                exec(execute)
        return cmd_list


def readybuild():
    """" Make Ready the bld structure If fusato exists remove it.
         and recreate it, otherwise just make a new folder named
         fusato.
    """
    logger.info("Making ready the fusato build directory")
    os.chdir(HOME_FOLDER)
    if os.path.exists('fusato'):
        os.system("sudo rm -r -f fusato")
    os.makedirs('fusato')


class Decisions:
    """
    Aguments used to tell the application what to build
    """
    def __init__(self):
        """ init the Decisions """
        # Set up the terminal arguments
        self.argument_parser = argparse.ArgumentParser()
        self.argument_parser.add_argument("set",
                                          help="set what build to start",
                                          type=str
                                          )
        self.argument_parser.add_argument("e",
                                          help="identify the desktop"
                                               " environment for example"
                                               " e-xfce",
                                          type=str
                                          )
        self.argument_parser.add_argument("b",
                                          help="identify the ISO to"
                                               "build for example"
                                               " b-deb64",
                                          type=str
                                          )
        self.arguments = self.argument_parser.parse_args()
        # Then determine what desktop build to look for
        desktop_build_function = getattr(self, f'd{self.arguments.e[2:]}', None)
        if desktop_build_function:
            desktop_build_function()

    def dxfce(self):
        """ Arguments for the XFCE """
        build_type_mapping = {
            'b-deb64': ("deb", "_64"),
            'b-deb32': ("deb", "_32"),
            'b-dev64': ("dev", "_64"),
            'b-dev32': ("dev", "_32"),
            'b-debarm': ("deb", "_arm"),
            'b-devarm': ("dev", "_arm"),
        }

        build_type_mapping_oem = {
            'b-deb64o': ("debo", "_64"),
            'b-deb32o': ("debo", "_32"),
            'b-dev64o': ("devo", "_64"),
            'b-dev32o': ("devo", "_32"),
            'b-debarmo': ("debo", "_arm"),
            'b-devarmo': ("devo", "_arm"),
        }

        build_argument = self.arguments.b
        if build_argument in build_type_mapping:
            readybuild()
            BuildXfce(*build_type_mapping[build_argument])
        else:
            logger.critical("You have not specified a build to process!")
        build_argument_oem = self.arguments.b
        if build_argument_oem in build_type_mapping_oem:
            readybuild()
            BuildXfce(*build_type_mapping[build_argument])
        else:
            logger.critical("You have not specified a build to process!")


    def dgnomefb(self):
        """ Arguments for the gnomefb """
        build_type_mapping = {
            'b-deb64': ("deb", "_64"),
            'b-deb32': ("deb", "_32"),
            'b-dev64': ("dev", "_64"),
            'b-dev32': ("dev", "_32"),
            'b-debarm': ("deb", "_arm"),
            'b-devarm': ("dev", "_arm"),
        }

        build_argument = self.arguments.b
        if build_argument in build_type_mapping:
            readybuild()
            BuildGflashback(*build_type_mapping[build_argument])
        else:
            logger.critical("You have not specified a build to process!")


    def dopenbox(self):
        """ Arguments for the openbox """
        build_type_mapping = {
            'b-deb64': ("deb", "_64"),
            'b-deb32': ("deb", "_32"),
            'b-dev64': ("dev", "_64"),
            'b-dev32': ("dev", "_32"),
            'b-debarm': ("deb", "_arm"),
            'b-devarm': ("dev", "_arm"),
        }

        build_argument = self.arguments.b
        if build_argument in build_type_mapping:
            pass
            readybuild()
            BuildOpenbox(*build_type_mapping[build_argument])
        else:
            logger.critical("You have not specified a build to process!")


    def dloadxf(self):
        """ Arguments for the loaded xfce """
        build_type_mapping = {
            'b-deb64': ("debld", "_64"),
            'b-dev64': ("devld", "_64"),
            'b-deb32': ("debld", "_32"),
            'b-dev32': ("devld", "_32"),
        }
        build_argument = self.arguments.b
        if build_argument in build_type_mapping:
            pass
            readybuild()
            BuildLoadedxf(*build_type_mapping[build_argument])
        else:
            logger.critical("You have not specified a build to process!")

    def dloadgfb(self):
        """ Arguments for the loaded Gnome flashback """
        build_type_mapping = {
            'b-deb64': ("debld", "_64"),
            'b-dev64': ("devld", "_64"),
            'b-deb32': ("debld", "_32"),
            'b-dev32': ("devld", "_32"),
        }
        build_argument = self.arguments.b
        if build_argument in build_type_mapping:
            pass
            readybuild()
            BuildLoadedgfb(*build_type_mapping[build_argument])
        else:
            logger.critical("You have not specified a build to process!")

    def dloadcin(self):
        """ Arguments for the loaded cinnamon """
        build_type_mapping = {
            'b-deb64': ("debld", "_64"),
            'b-dev64': ("devld", "_64"),
            'b-deb32': ("debld", "_32"),
            'b-dev32': ("devld", "_32"),
        }
        build_argument = self.arguments.b
        if build_argument in build_type_mapping:
            pass
            readybuild()
            BuildLoadedcin(*build_type_mapping[build_argument])
        else:
            logger.critical("You have not specified a build to process!")

    def dserver(self):
        """ Arguments for the Server """
        build_type_mapping = {
            'b-deb64': ("debsrv", "_64"),
            'b-dev64': ("devsrv", "_64"),
        }

        build_argument = self.arguments.b
        if build_argument in build_type_mapping:
            pass
            readybuild()
            BuildServer(*build_type_mapping[build_argument])
        else:
            logger.critical("You have not specified a build to process!")


    def dmini(self):
        """ Arguments for the Mini """
        build_type_mapping = {
            'b-deb64': ("debmin", "_64"),
            'b-deb32': ("debmin", "_32"),
            'b-dev64': ("devmin", "_64"),
            'b-dev32': ("devmin", "_32"),
        }

        build_argument = self.arguments.b
        if build_argument in build_type_mapping:
            pass
            readybuild()
            BuildMini(*build_type_mapping[build_argument])
        else:
            logger.critical("You have not specified a build to process!")


Decisions()

"""
* Author: "PeppermintOS Team(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* Used to call functions for final runs as needed
"""
import finish_cleanup

finish_cleanup.kill_fusato()

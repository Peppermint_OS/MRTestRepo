"""
* Author: "PeppermintOS Team(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* These functions will build the base of the fusato build folders
* As well as commonly used functions
"""

import os
from pathlib import Path
import conf

# Set the home path used regardless the user logged in
BSTRING_ISO_CONFIGS = '~/bubbles/iso_configs'
HOME_FOLDER = str(Path(BSTRING_ISO_CONFIGS).expanduser())
PACKAGE_LIST = "fusato/config/package-lists/"
CHROOT_FOLDER = "fusato/config/includes.chroot/"
CHROOT_INSTALLER = "fusato/config/includes.chroot/"
INCLUDES_INSTALLER = "fusato/config/includes.installer/"
BOOTSTRAP_FOLDER = 'fusato/config/includes.bootstrap/'
FUSATO_ROOT = 'fusato'

# Fusato base
def set_fusato_structure():
    """ Make some needed folders for the fusato build process.
        at the moment you deal with chroot and bootstrap
    """
    make_chroot = ['usr/share/distro-info/',
                   'usr/share/python-apt/templates/',
                   'usr/share/icons/default',
                   'usr/share/peppermint/',
                   'usr/share/themes/',
                   'usr/local/bin/',
                   'usr/bin/',
                   'usr/sbin',
                   'etc/lightdm',
                   'etc/default',
                   'etc/apt',
                   'etc/apt/preferences.d',
                   'etc/apt/sources.list.d',
                   'etc/skel/Desktop',
                   'etc/skel/.local/share',
                   'etc/skel/.config/autostart/',
                   'etc/lightdm/lightdm.conf.d/',
                   'etc/plymouth',
                   ]
    os.chdir(os.path.join(HOME_FOLDER, FUSATO_ROOT))
    if os.path.exists('config'):
        rm_cmd = "sudo rm -r -f config"
        os.system(rm_cmd)
        os.makedirs('config')

    make_bootstrap = ['etc/apt']
    make_packages = ['package-lists/']
    make_chfldrs = make_chroot
    for f_f in make_chfldrs:
        os.makedirs(os.path.join(HOME_FOLDER, CHROOT_FOLDER, f_f))
    for f_f in make_bootstrap:
        os.makedirs(os.path.join(HOME_FOLDER, BOOTSTRAP_FOLDER, f_f))
    for f_f in make_packages:
        os.makedirs(os.path.join(HOME_FOLDER, PACKAGE_LIST, f_f))


def set_fusato_server_structure():
    """ Make some needed folders for the fusato build process.
        at the moment you deal with chroot, bootstrap, and
        includes-installer
    """
    make_chroot = ['etc/firewalld/zones',
                   'boot/grub',
                   ]
    os.chdir(os.path.join(HOME_FOLDER, FUSATO_ROOT))
    if os.path.exists('config'):
        rm_cmd = "sudo rm -r -f config"
        os.system(rm_cmd)
        os.makedirs('config')

    make_bootstrap = ['etc/apt']
    make_packages = ['package-lists/']
    make_chfldrs = make_chroot
    for f_f in make_chfldrs:
        os.makedirs(os.path.join(HOME_FOLDER, CHROOT_FOLDER, f_f))
    for f_f in make_bootstrap:
        os.makedirs(os.path.join(HOME_FOLDER, BOOTSTRAP_FOLDER, f_f))
    for f_f in make_packages:
        os.makedirs(os.path.join(HOME_FOLDER, PACKAGE_LIST, f_f))


def set_fusato_server_installer_structure():
    """ Make some needed folders for the fusato build process.
        at the moment you deal with the installer
    """
    # Define the directories to be created in includes-installer
    installer_dirs = ['/preseed',
                      '/usr/lib/finish-install.d/',
                      '/usr/share/'
                      ]

    # Create the directories in includes-installer
    for installer_dir in installer_dirs:
        full_path = os.path.join(HOME_FOLDER, INCLUDES_INSTALLER, 
            installer_dir.strip('/')
            )
        os.makedirs(full_path, exist_ok=True)
        print(f"Created directory: {full_path}")


def set_fusato_mini_installer_structure():
    """ Make some needed folders for the fusato build process.
        at the moment you deal with the installer
    """
    # Define the directories to be created in includes-installer
    installer_dirs = ['/preseed',
                      '/usr/lib/finish-install.d/',
                      '/usr/share/',
                      '/preseed/repos/',
                      '/preseed/keyrings/',
                      '/preseed/grub/',
                      '/preseed/apps/',
                      '/preseed/database/',
                      '/preseed/pixmaps/',
                      '/preseed/tools/peptools/images',
                      '/preseed/tools/peptools/__pycache__',
                      '/preseed/tools/peptools/ttkcreator',
                      '/preseed/protools/',
                      '/preseed/polkit/',
                      '/preseed/conf/',
                      '/preseed/py/ttkbootstrap-1.10.1.dist-info/',
                      '/preseed/py/requests/',
                      '/preseed/py/tendo-0.3.0.dist-info/',
                      '/preseed/py/ttkcreator/__pycache__/',
                      '/preseed/py/ttkbootstrap/dialogs/__pycache__/',
                      '/preseed/py/ttkbootstrap/localization/__pycache__/',
                      '/preseed/py/ttkbootstrap/__pycache__/',
                      '/preseed/py/ttkbootstrap/themes/__pycache__/',
                      '/preseed/py/tendo/tests/__pycache__/',
                      '/preseed/py/tendo/tests/assets/__pycache__/',
                      '/preseed/py/tendo/__pycache__/',
                      '/preseed/lightdm/',
                      '/preseed/autostart/',
                      '/etc/'
                      ]

# Create the directories in includes-installer
    for installer_dir in installer_dirs:
        full_path = os.path.join(HOME_FOLDER, INCLUDES_INSTALLER, 
            installer_dir.strip('/')
            )
        os.makedirs(full_path, exist_ok=True)
        print(f"Created directory: {full_path}")


def set_fusato_installer_structure():
    """ Make some needed folders for the fusato build process.
        at the moment you deal with chroot and bootstrap
    """
    make_chroot = ['/etc/calamares/',
                   '/etc/calamares/modules/',
                   '/etc/calamares/branding/',
                   '/usr/bin/',
                   '/usr/sbin/',
                   '/usr/lib/calamares/modules',
                   '/usr/share/glib-2.0/schemas',
                   ]

    # Change directory to the correct path
    try:
        os.chdir(os.path.join(HOME_FOLDER, FUSATO_ROOT))
    except Exception as e:
        print(f"Error changing directory: {e}")
        return

    for f_f in make_chroot:
        full_path = os.path.join(HOME_FOLDER, CHROOT_FOLDER.strip('/'),
            f_f.strip('/')
            )
        try:
            os.makedirs(full_path, exist_ok=True)
            print(f"Created directory: {full_path}")
        except Exception as e:
            print(f"Error creating directory {full_path}: {e}")


## This is not really needed asthif ile is not executed 
##if __name__ == "__main__":
##    set_fusato_installer_structure()

# Commonly Shared
def make_build_file(base, arch, extension, build_description):
    """
    This will get the base, arch, extension, and build, to write the
    file
    """
    file_path = os.path.join(os.path.expanduser(HOME_FOLDER), 
        FUSATO_ROOT, f'{base}.{arch}{extension}'
        )
    with open(file_path, 'w', encoding='UTF-8') as f_p:
        f_p.write(build_description)


def make_bld_xfce(base, arch):
    """Write the xfce build file"""
    make_build_file(base, arch, 'xfc', 'XFCE Build')


def make_bld_gnomeflashback(base, arch):
    """Write the gnome fb build file"""
    make_build_file(base, arch, 'gfb', 'Gnome Flash Back Build')


def make_bld_openbox(base, arch):
    """Write the openbox build file"""
    make_build_file(base, arch, 'opb', 'OpenBox Build')


def make_bld_loaded(base, arch):
    """Write the loaded build file"""
    make_build_file(base, arch, 'loaded', 'Loaded Build')


def make_bld_server(base, arch):
    """Write the server build file"""
    make_build_file(base, arch, 'server', 'Server Build')


def make_bld_mini(base, arch):
    """Write the mini build file"""
    make_build_file(base, arch, 'mini', 'Mini Build')


# Make the shared package lists files
def make_package_list(file_name, content, mode='a'):
    """Create the package list file with the specified content if it
        doesn't exist.
    """
    file_path = os.path.join(HOME_FOLDER, PACKAGE_LIST, file_name)
    if not os.path.exists(file_path):
        with open(file_path, mode, encoding='UTF-8') as f_p:
            f_p.write(content)
            print(f"Created package list: {file_path}")
    else:
        print(f"Package list already exists: {file_path}")


def set_general_shared():
    """ Create the list for general shared list"""
    make_package_list('genshared.list.chroot',
        conf.GENERAL_SHARED_LIST, mode='x'
        )


def set_specific_32_packages():
    """Create the list for the 32bit flagship"""
    make_package_list('flagspec32.list.chroot',
        conf.FLAG_SPECIFIC_LIST_32, mode='x'
        )


def set_grub_shared():
    """ Create the list for shared grub list"""
    make_package_list('grub.list.chroot',
        conf.GRUB_LIST_SHARED, mode='x'
    )


def set_binary_shared():
    """ Create the shared list for grub binary"""
    make_package_list('installer.list.binary',
        conf.BINARY_LIST_SHARED, mode='x'
        )


# Light DM package list login window
def set_lightdm():
    """ Create the list for the light dm list  """
    make_package_list('lightdm.list.chroot',
        conf.LIGHT_DM_LIST, mode='x'
        )

# Desktop Environments
def set_xfce():
    """ Create the list for the xfce xfce list"""
    make_package_list('xfce.list.chroot',
        conf.XFCE_LIST, mode='x'
        )


def set_gfb():
    """ Create the list for the gnomeflashback list"""
    make_package_list('gfb.list.chroot',
        conf.GNOME_FLASHBACK_LIST, mode='x'
        )


def set_opb():
    """ Create the list for the openbox list"""
    make_package_list('opb.list.chroot',
        conf.OPENBOX_LIST, mode='x'
        )


def set_loadedxf():
    """ Create the list for the loaded xfce list"""
    make_package_list('loaded.list.chroot',
        conf.LOADED_XFCE_LIST, mode='x'
        )

def set_loadedgfb():
    """ Create the list for the loaded kde list"""
    make_package_list('loaded.list.chroot',
        conf.LOADED_GFB_LIST, mode='x'
        )

def set_loadedcin():
    """ Create the list for the loaded cinnamon list"""
    make_package_list('loaded.list.chroot',
        conf.LOADED_CINNAMON_LIST, mode='x'
        )

# Server Specific
def set_server():
    """ Create the list for the server list"""
    make_package_list('server.list.chroot',
        conf.SERVER_LIST, mode='x'
        )


# Mini Specific
def set_mini():
    """ Create the list for the loaded list"""
    make_package_list('mini.list.chroot',
        conf.MINI_LIST, mode='x'
        )


# CHROOT Specific
def set_chroot_grub_64():
    """ Append the grub list for 64 bit grub"""
    make_package_list('gfb.list.chroot', conf.GRUB_LIST_64)


def set_chroot_grub_arm():
    """ Append the grub list for the ARM grub"""
    make_package_list('gfb.list.chroot', conf.GRUB_LIST_ARM64)


def set_chroot_grub_32():
    """ Append the grub list for the 32 bit grub"""
    make_package_list('gfb.list.chroot', conf.GRUB_LIST_32)


# Binary Specific
def set_binary_64():
    """ Create the list for grub binary packages for 64 bit"""
    make_package_list('installer.list.binary', conf.BINARY_LIST_64)


def set_binary_arm():
    """ Create the list for grub binary packages for 64 bit"""
    make_package_list('installer.list.binary', conf.BINARY_LIST_ARM)


def set_binary_32():
    """ Create the list for grub binary packages for 32 bit"""
    make_package_list('installer.list.binary', conf.BINARY_LIST_32)


# Firmware Specific
def set_firmware():
    """ Create the list for the firmware support for general ISOs"""
    make_package_list('firmware.list.chroot',
        conf.FIRMWARE_LIST_32_64, mode='x'
        )


def set_firmware_arm():
    """ Create the list for the firmware support for arm ISOs"""
    make_package_list('firmware.list.chroot',
        conf.FIRMWARE_LIST_ARM, mode='x'
        )


"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* Use this file to add any git sources that you wan to monitor 
"""


from buildbot.plugins import *

def setup_change_source(c):
    """
    This def is used to add a git source.
    """
    c['change_source'] = []
    c['change_source'].append(changes.GitPoller( 'https://github.com/buildbot/hello-world.git', workdir='gitpoller-workdir', branch='master', pollInterval=300))

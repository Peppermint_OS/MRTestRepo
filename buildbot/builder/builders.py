"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* Use this file,  to add your builders just add them after the one before it.
* Use the factory from factories.py
"""


from buildbot.plugins import *
from factories import *

example_factory = create_example_factory()

# These are the factories for the mainlines
deb_64xfce_factory = deb_64xfce_factory()
deb_32xfce_factory = deb_32xfce_factory()
dev_64xfce_factory = dev_64xfce_factory()
dev_32xfce_factory = dev_32xfce_factory()
deb_armxfce_factory = deb_armxfce_factory()
dev_armxfce_factory = dev_armxfce_factory()
deb_64gfb_factory = deb_64gfb_factory()
deb_32gfb_factory = deb_32gfb_factory()
dev_64gfb_factory = dev_64gfb_factory()
dev_32gfb_factory = dev_32gfb_factory()
deb_64opb_factory = deb_64opb_factory()
deb_32opb_factory = deb_32opb_factory()
dev_64opb_factory = dev_64opb_factory()
dev_32opb_factory = dev_32opb_factory()

# Loaded builds factory
loadedxf_deb64_factory = loadedxf_deb64_factory()
loadedxf_dev64_factory = loadedxf_dev64_factory()
loadedxf_deb32_factory = loadedxf_deb32_factory()
loadedxf_dev32_factory = loadedxf_dev32_factory()
loadedgfb_deb64_factory = loadedgfb_deb64_factory()
loadedgfb_dev64_factory = loadedgfb_dev64_factory()
loadedgfb_deb32_factory = loadedgfb_deb32_factory()
loadedgfb_dev32_factory = loadedgfb_dev32_factory()
loadedcin_deb64_factory = loadedcin_deb64_factory()
loadedcin_dev64_factory = loadedcin_dev64_factory()
loadedcin_deb32_factory = loadedcin_deb32_factory()
loadedcin_dev32_factory = loadedcin_dev32_factory()

# Server builds factory
server_deb64_factory = server_deb64_factory()
server_dev64_factory = server_dev64_factory()

# Mini builds factory
mini_deb64_factory = mini_deb64_factory()
mini_deb32_factory = mini_deb32_factory()
mini_dev64_factory = mini_dev64_factory()
mini_dev32_factory = mini_dev32_factory()

builders = [
	# XFCE Builders
    util.BuilderConfig( name="runtests", workernames=["local_worker"], factory=example_factory),
    util.BuilderConfig(name="Deb_64_XFCE", workernames=["local_worker"], factory=deb_64xfce_factory),
    util.BuilderConfig(name="Deb_32_XFCE", workernames=["local_worker"], factory=deb_32xfce_factory),
    util.BuilderConfig(name="Dev_64_XFCE", workernames=["local_worker"], factory=dev_64xfce_factory),
    util.BuilderConfig(name="Dev_32_XFCE", workernames=["local_worker"], factory=dev_32xfce_factory),
    util.BuilderConfig(name="Deb_ARM_XFCE", workernames=["local_worker"], factory=deb_armxfce_factory),
    util.BuilderConfig(name="Dev_ARM_XFCE", workernames=["local_worker"], factory=dev_armxfce_factory),
	# Gnome Flash Back Builders
    util.BuilderConfig(name="Deb_64_GNFB", workernames=["local_worker"], factory=deb_64gfb_factory),
    util.BuilderConfig(name="Deb_32_GNFB", workernames=["local_worker"], factory=deb_32gfb_factory),
    util.BuilderConfig(name="Dev_64_GNFB", workernames=["local_worker"], factory=dev_64gfb_factory),
    util.BuilderConfig(name="Dev_32_GNFB", workernames=["local_worker"], factory=dev_32gfb_factory),
	# Open Box Builders
    util.BuilderConfig(name="Deb_64_OPB", workernames=["local_worker"], factory=deb_64opb_factory),
    util.BuilderConfig(name="Deb_32_OPB", workernames=["local_worker"], factory=deb_32opb_factory),
    util.BuilderConfig(name="Dev_64_OPB", workernames=["local_worker"], factory=dev_64opb_factory),
    util.BuilderConfig(name="Dev_32_OPB", workernames=["local_worker"], factory=dev_32opb_factory),
	# Loaded Builders
    util.BuilderConfig(name="Loadedxf_DEB64", workernames=["local_worker"], factory=loadedxf_deb64_factory),
    util.BuilderConfig(name="Loadedxf_DEV64", workernames=["local_worker"], factory=loadedxf_dev64_factory),
    util.BuilderConfig(name="Loadedxf_DEB32", workernames=["local_worker"], factory=loadedxf_deb32_factory),
    util.BuilderConfig(name="Loadedxf_DEV32", workernames=["local_worker"], factory=loadedxf_dev32_factory),
    util.BuilderConfig(name="Loadedgfb_DEB64", workernames=["local_worker"], factory=loadedgfb_deb64_factory),
    util.BuilderConfig(name="Loadedgfb_DEV64", workernames=["local_worker"], factory=loadedgfb_dev64_factory),
    util.BuilderConfig(name="Loadedgfb_DEB32", workernames=["local_worker"], factory=loadedgfb_deb32_factory),
    util.BuilderConfig(name="Loadedgfb_DEV32", workernames=["local_worker"], factory=loadedgfb_dev32_factory),
    util.BuilderConfig(name="Loadedcin_DEB64", workernames=["local_worker"], factory=loadedcin_deb64_factory),
    util.BuilderConfig(name="Loadedcin_DEV64", workernames=["local_worker"], factory=loadedcin_dev64_factory),
    util.BuilderConfig(name="Loadedcin_DEB32", workernames=["local_worker"], factory=loadedcin_deb32_factory),
    util.BuilderConfig(name="Loadedcin_DEV32", workernames=["local_worker"], factory=loadedcin_dev32_factory),
	# Server Builders
    util.BuilderConfig(name="Server_DEB64", workernames=["local_worker"], factory=server_deb64_factory),
    util.BuilderConfig(name="Server_DEV64", workernames=["local_worker"], factory=server_dev64_factory),
	# Mini Builders
    util.BuilderConfig(name="Mini_DEB64", workernames=["local_worker"], factory=mini_deb64_factory),
    util.BuilderConfig(name="Mini_DEV64", workernames=["local_worker"], factory=mini_dev64_factory),
    util.BuilderConfig(name="Mini_DEB32", workernames=["local_worker"], factory=mini_deb32_factory),
    util.BuilderConfig(name="Mini_DEV32", workernames=["local_worker"], factory=mini_dev32_factory),
]

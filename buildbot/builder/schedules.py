"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* Use this file, to add the schedules that are used for scheduling
"""

from buildbot.plugins import *

def setup_schedules(c):
    """
    Add your schedules below
    """
    c['schedulers'] = []
    c['schedulers'].append(schedulers.SingleBranchScheduler(name="all", change_filter=util.ChangeFilter(branch='master'), treeStableTimer=None,builderNames=["runtests"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="force",builderNames=["runtests"]))
    # ----- Manual Schedules ----- #
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Deb_32_XFCE", builderNames=["Deb_32_XFCE"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Deb_64_XFCE", builderNames=["Deb_64_XFCE"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Dev_64_XFCE", builderNames=["Dev_64_XFCE"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Dev_32_XFCE", builderNames=["Dev_32_XFCE"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Deb_ARM_XFCE", builderNames=["Deb_ARM_XFCE"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Dev_ARM_XFCE", builderNames=["Dev_ARM_XFCE"]))
    #----- Gnome Flashback -  Main Stream Builds ----- #
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Deb_32_GNFB", builderNames=["Deb_32_GNFB"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Deb_64_GNFB", builderNames=["Deb_64_GNFB"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Dev_64_GNFB", builderNames=["Dev_64_GNFB"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Dev_32_GNFB", builderNames=["Dev_32_GNFB"]))
    #----- Openbox -  Main Stream Builds -----#
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Deb_32_OPB", builderNames=["Deb_32_OPB"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Deb_64_OPB", builderNames=["Deb_64_OPB"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Dev_64_OPB", builderNames=["Dev_64_OPB"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Dev_32_OPB", builderNames=["Dev_32_OPB"]))
    # ----- Loaded -  Builds -----#
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedxf_DEB64", builderNames=["Loadedxf_DEB64"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedxf_DEV64", builderNames=["Loadedxf_DEV64"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedxf_DEB32", builderNames=["Loadedxf_DEB32"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedxf_DEV32", builderNames=["Loadedxf_DEV32"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedgfb_DEB64", builderNames=["Loadedgfb_DEB64"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedgfb_DEV64", builderNames=["Loadedgfb_DEV64"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedgfb_DEB32", builderNames=["Loadedgfb_DEB32"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedgfb_DEV32", builderNames=["Loadedgfb_DEV32"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedcin_DEB64", builderNames=["Loadedcin_DEB64"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedcin_DEV64", builderNames=["Loadedcin_DEV64"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedcin_DEB32", builderNames=["Loadedcin_DEB32"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Loadedcin_DEV32", builderNames=["Loadedcin_DEV32"]))
    # ----- Server -  Builds -----#
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Server_DEB64", builderNames=["Server_DEB64"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Server_DEV64", builderNames=["Server_DEV64"]))
    # ----- Server -  Builds -----#
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Mini_DEB64", builderNames=["Mini_DEB64"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Mini_DEV64", builderNames=["Mini_DEV64"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Mini_DEB32", builderNames=["Mini_DEB32"]))
    c['schedulers'].append(schedulers.ForceScheduler(name="Manual_Mini_DEV32", builderNames=["Mini_DEV32"]))


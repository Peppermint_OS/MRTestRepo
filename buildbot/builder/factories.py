"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* Each def is a factory as you need more factories  just
* create a new dev named what you need  then use that def name
* in the  master.cfg
"""

from buildbot.plugins import *

def create_example_factory():
    """ This is the example Factory used for  the example job """
    factory = util.BuildFactory()
    factory.addStep(steps.Git(repourl='https://github.com/buildbot/hello-world.git', mode='incremental'))
    factory.addStep(steps.ShellCommand(command=["trial", "hello"], env={"PYTHONPATH": "."}))
    return factory


def deb_64xfce_factory():
    """ XFCE 64 Debian """
    deb_64xfce_factory = util.BuildFactory()
    deb_64xfce_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    deb_64xfce_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/deb.64xfc"]))
    deb_64xfce_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return deb_64xfce_factory


def deb_32xfce_factory():
    """ XFCE 32 Debian """
    deb_32xfce_factory = util.BuildFactory()
    deb_32xfce_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    deb_32xfce_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/deb.32xfc"]))
    deb_32xfce_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return deb_32xfce_factory


def dev_64xfce_factory():
    """ XFCE 64 Devuan"""
    dev_64xfce_factory = util.BuildFactory()
    dev_64xfce_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    dev_64xfce_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/dev.64xfc"]))
    dev_64xfce_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return dev_64xfce_factory


def dev_32xfce_factory():
    """ XFCE 32 Devuan """
    dev_32xfce_factory = util.BuildFactory()
    dev_32xfce_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    dev_32xfce_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/dev.32xfc"]))
    dev_32xfce_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return dev_32xfce_factory


def deb_armxfce_factory():
    """ XFCE ARM  Debian """
    deb_armxfce_factory = util.BuildFactory()
    deb_armxfce_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    deb_armxfce_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/deb.armxfc"]))
    deb_armxfce_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return deb_armxfce_factory


def dev_armxfce_factory():
    """ XFCE ARM Devuan """
    dev_armxfce_factory = util.BuildFactory()
    dev_armxfce_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    dev_armxfce_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/dev.armxfc"]))
    dev_armxfce_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return dev_armxfce_factory


def deb_64gfb_factory():
    """ Gnome Flash back 64 Debian """
    deb_64gfb_factory = util.BuildFactory()
    deb_64gfb_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    deb_64gfb_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/deb.64gfb"]))
    deb_64gfb_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return deb_64gfb_factory


def deb_32gfb_factory():
    """ Gnome Flash back 32 Debian """
    deb_32gfb_factory = util.BuildFactory()
    deb_32gfb_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    deb_32gfb_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/deb.32gfb"]))
    deb_32gfb_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return deb_32gfb_factory


def dev_64gfb_factory():
    """" Gnome Flash back 64 Devuan """
    dev_64gfb_factory = util.BuildFactory()
    dev_64gfb_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    dev_64gfb_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/dev.64gfb"]))
    dev_64gfb_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return dev_64gfb_factory


def dev_32gfb_factory():
    """ Gnome Flash back 32 Devuan """
    dev_32gfb_factory = util.BuildFactory()
    dev_32gfb_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    dev_32gfb_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/dev.32gfb"]))
    dev_32gfb_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return dev_32gfb_factory


def deb_64opb_factory():
    """ Open Box 64 Debian """
    deb_64opb_factory = util.BuildFactory()
    deb_64opb_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    deb_64opb_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/deb.64opb"]))
    deb_64opb_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return deb_64opb_factory


def deb_32opb_factory():
    """ Open Box 32 Debian """
    deb_32opb_factory = util.BuildFactory()
    deb_32opb_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    deb_32opb_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/deb.32opb"]))
    deb_32opb_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return deb_32opb_factory


def dev_64opb_factory():
    """ Open Box 64 Devuan """
    dev_64opb_factory = util.BuildFactory()
    dev_64opb_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    dev_64opb_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/dev.64opb"]))
    dev_64opb_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return dev_64opb_factory


def dev_32opb_factory():
    """ Open Box 32 Devuan """
    dev_32opb_factory = util.BuildFactory()
    dev_32opb_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    dev_32opb_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/dev.32opb"]))
    dev_32opb_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return dev_32opb_factory

	# Loaded builds
def loadedxf_deb64_factory():
    """ LOADED XFCE 64 Debian """
    loadedxf_deb64_factory = util.BuildFactory()
    loadedxf_deb64_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedxf_deb64_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/debld.64loadedxf"]))
    loadedxf_deb64_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedxf_deb64_factory


def loadedxf_dev64_factory():
    """ LOADED XFCE 64 Devuan """
    loadedxf_dev64_factory = util.BuildFactory()
    loadedxf_dev64_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedxf_dev64_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/devld.64loadedxf"]))
    loadedxf_dev64_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedxf_dev64_factory

def loadedxf_deb32_factory():
    """ LOADED XFCE 32 Debian """
    loadedxf_deb32_factory = util.BuildFactory()
    loadedxf_deb32_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedxf_deb32_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/debld.32loadedxf"]))
    loadedxf_deb32_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedxf_deb32_factory


def loadedxf_dev32_factory():
    """ LOADED XFCE 32 Devuan """
    loadedxf_dev32_factory = util.BuildFactory()
    loadedxf_dev32_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedxf_dev32_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/devld.32loadedxf"]))
    loadedxf_dev32_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedxf_dev32_factory

def loadedgfb_deb64_factory():
    """ LOADED KDE 64 Debian """
    loadedgfb_deb64_factory = util.BuildFactory()
    loadedgfb_deb64_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedgfb_deb64_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/debld.64loadedgfb"]))
    loadedgfb_deb64_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedgfb_deb64_factory


def loadedgfb_dev64_factory():
    """ LOADED KDE 64 Devuan """
    loadedgfb_dev64_factory = util.BuildFactory()
    loadedgfb_dev64_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedgfb_dev64_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/devld.64loadedgfb"]))
    loadedgfb_dev64_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedgfb_dev64_factory

def loadedgfb_deb32_factory():
    """ LOADED KDE 32 Debian """
    loadedgfb_deb32_factory = util.BuildFactory()
    loadedgfb_deb32_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedgfb_deb32_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/debld.32loadedgfb"]))
    loadedgfb_deb32_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedgfb_deb32_factory


def loadedgfb_dev32_factory():
    """ LOADED KDE 32 Devuan """
    loadedgfb_dev32_factory = util.BuildFactory()
    loadedgfb_dev32_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedgfb_dev32_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/devld.32loadedgfb"]))
    loadedgfb_dev32_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedgfb_dev32_factory

def loadedcin_deb64_factory():
    """ LOADED CINNAMON 64 Debian """
    loadedcin_deb64_factory = util.BuildFactory()
    loadedcin_deb64_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedcin_deb64_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/debld.64loadedcin"]))
    loadedcin_deb64_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedcin_deb64_factory


def loadedcin_dev64_factory():
    """ LOADED CINNAMON 64 Devuan """
    loadedcin_dev64_factory = util.BuildFactory()
    loadedcin_dev64_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedcin_dev64_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/devld.64loadedcin"]))
    loadedcin_dev64_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedcin_dev64_factory

def loadedcin_deb32_factory():
    """ LOADED CINNAMON 32 Debian """
    loadedcin_deb32_factory = util.BuildFactory()
    loadedcin_deb32_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedcin_deb32_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/debld.32loadedcin"]))
    loadedcin_deb32_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedcin_deb32_factory


def loadedcin_dev32_factory():
    """ LOADED CINNAMON 32 Devuan """
    loadedcin_dev32_factory = util.BuildFactory()
    loadedcin_dev32_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    loadedcin_dev32_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/devld.32loadedcin"]))
    loadedcin_dev32_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return loadedcin_dev32_factory

	# Server builds
def server_deb64_factory():
    """ SERVER 64 Debian"""
    server_deb64_factory = util.BuildFactory()
    server_deb64_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    server_deb64_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/debsrv.64server"]))
    server_deb64_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return server_deb64_factory


def server_dev64_factory():
    """ SERVER 64 Devuan """
    server_dev64_factory = util.BuildFactory()
    server_dev64_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    server_dev64_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/devsrv.64server"]))
    server_dev64_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return server_dev64_factory

	# Mini builds
def mini_deb64_factory():
    """ MINI 64  Debian """
    mini_deb64_factory = util.BuildFactory()
    mini_deb64_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    mini_deb64_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/debmin.64mini"]))
    mini_deb64_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return mini_deb64_factory


def mini_deb32_factory():
    """ MINI 32 Debian """
    mini_deb32_factory = util.BuildFactory()
    mini_deb32_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    mini_deb32_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/debmin.32mini"]))
    mini_deb32_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return mini_deb32_factory


def mini_dev64_factory():
    """ MINI 64 Devuan """
    mini_dev64_factory = util.BuildFactory()
    mini_dev64_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    mini_dev64_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/devmin.64mini"]))
    mini_dev64_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return mini_dev64_factory


def mini_dev32_factory():
    """ MINI 32 Devuan """
    mini_dev32_factory = util.BuildFactory()
    mini_dev32_factory.addStep(steps.ShellCommand(command=["/home/pepadmin/start/clean.sh"]))
    mini_dev32_factory.addStep(steps.ShellCommand(command=["touch", "/home/pepadmin/start/bldtype/devmin.32mini"]))
    mini_dev32_factory.addStep(steps.Compile(command=["python3", "/home/pepadmin/start/ssh_build.py"], timeout=None))
    return mini_dev32_factory



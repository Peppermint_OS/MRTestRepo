"""
* Author: "PeppermintOS Team(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* Move to bubbles and start the build. This is used to assist
* Buildbot
"""

import os
from pathlib import Path


START_LOCATION = '/iso-builder/pepadmin/start/bldtype'
WORKING_FOLDER = str(Path(START_LOCATION).expanduser())
os.chdir(WORKING_FOLDER)

def runxfce ():
    """Run the XFCE builds as needed"""
    sshx = "ssh manuel@manuel-pc2 "
    if os.path.exists('deb.64xfc'):
        cmd ="'cd /home/pepadmin/bubbles; make deb64xfce'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('deb.32xfc'):
        cmd ="'cd /home/pepadmin/bubbles; make deb32xfce'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('dev.64xfc'):
        cmd ="'cd /home/pepadmin/bubbles; make dev64xfce'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('dev.32xfc'):
        cmd ="'cd /home/pepadmin/bubbles; make dev32xfce'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('deb.armxfc'):
        cmd ="'cd /home/pepadmin/bubbles; make debarmxfce'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('dev.armxfc'):
        cmd ="'cd /home/pepadmin/bubbles; make devarmxfce'"
        run_build = sshx + cmd
        os.system(run_build)
    else:
        print("You are not building XFCE!")



def rungfb ():
    """Run the GnomeFlashback builds as needed"""
    sshx = "ssh manuel@manuel-pc2 "
    if os.path.exists('deb.64gfb'):
        cmd ="'cd /home/pepadmin/bubbles; make deb64gfb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('deb.32gfb'):
        cmd ="'cd /home/pepadmin/bubbles; make deb32gfb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('dev.64gfb'):
        cmd ="'cd /home/pepadmin/bubbles; make dev64gfb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('dev.32gfb'):
        cmd ="'cd /home/pepadmin/bubbles; make dev32gfb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('deb.armgfb'):
        cmd ="'cd /home/pepadmin/bubbles; make debarmgfb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('dev.armgfb'):
        cmd ="'cd /home/pepadmin/bubbles; make devarmgfb'"
        run_build = sshx + cmd
        os.system(run_build)
    else:
        print("You are not building GnomeFlashBack!")



def runopb ():
    """Run the OpenBox builds as needed"""
    sshx = "ssh manuel@manuel-pc2 "
    if os.path.exists('deb.64opb'):
        cmd ="'cd /home/pepadmin/bubbles; make deb64opb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('deb.32opb'):
        cmd ="'cd /home/pepadmin/bubbles; make deb32opb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('dev.64opb'):
        cmd ="'cd /home/pepadmin/bubbles; make dev64opb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('dev.32opb'):
        cmd ="'cd /home/pepadmin/bubbles; make dev32opb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('deb.armgfb'):
        cmd ="'cd /home/pepadmin/bubbles; make debarmopb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('dev.armgfb'):
        cmd ="'cd /home/pepadmin/bubbles; make devarmopb'"
        run_build = sshx + cmd
        os.system(run_build)
    else:
        print("You are Not building OpenBox!")


def runloadedxf ():
    """Run the Loaded xfce builds as needed"""
    sshx = "ssh manuel@manuel-pc2 "
    if os.path.exists('debld.64loadedxf'):
        cmd ="'cd /home/pepadmin/bubbles; make deb64loadxf'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('devld.64loadedxf'):
        cmd ="'cd /home/pepadmin/bubbles; make dev64loadxf'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('debld.32loadedxf'):
        cmd ="'cd /home/pepadmin/bubbles; make deb32loadxf'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('devld.32loadedxf'):
        cmd ="'cd /home/pepadmin/bubbles; make dev32loadxf'"
        run_build = sshx + cmd
        os.system(run_build)
    else:
        print("You are Not building The Loaded ISOS!")

def runloadedgfb ():
    """Run the Loaded kde builds as needed"""
    sshx = "ssh manuel@manuel-pc2 "
    if os.path.exists('debld.64loadedgfb'):
        cmd ="'cd /home/pepadmin/bubbles; make deb64loadgfb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('devld.64loadedgfb'):
        cmd ="'cd /home/pepadmin/bubbles; make dev64loadgfb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('debld.32loadedgfb'):
        cmd ="'cd /home/pepadmin/bubbles; make deb32loadgfb'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('devld.32loadedgfb'):
        cmd ="'cd /home/pepadmin/bubbles; make dev32loadgfb'"
        run_build = sshx + cmd
        os.system(run_build)
    else:
        print("You are Not building The Loaded ISOS!")

def runloadedcin ():
    """Run the Loaded cinnamon builds as needed"""
    sshx = "ssh manuel@manuel-pc2 "
    if os.path.exists('debld.64loadedcin'):
        cmd ="'cd /home/pepadmin/bubbles; make deb64loadcin'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('devld.64loadedcin'):
        cmd ="'cd /home/pepadmin/bubbles; make dev64loadcin'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('debld.32loadedcin'):
        cmd ="'cd /home/pepadmin/bubbles; make deb32loadcin'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('devld.32loadedcin'):
        cmd ="'cd /home/pepadmin/bubbles; make dev32loadcin'"
        run_build = sshx + cmd
        os.system(run_build)
    else:
        print("You are Not building The Loaded cinnamon ISOS!")

def runserver ():
    """Run the Server builds as needed"""
    sshx = "ssh manuel@manuel-pc2 "
    if os.path.exists('debsrv.64server'):
        cmd ="'cd /home/pepadmin/bubbles; make debserver'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('devsrv.64server'):
        cmd ="'cd /home/pepadmin/bubbles; make devserver'"
        run_build = sshx + cmd
        os.system(run_build)
    else:
        print("You are Not building The Server ISOS!")


def runmini ():
    """Run the Mini builds as needed"""
    sshx = "ssh manuel@manuel-pc2 "
    if os.path.exists('debmin.64mini'):
        cmd ="'cd /home/pepadmin/bubbles; make deb64mini'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('debmin.32mini'):
        cmd ="'cd /home/pepadmin/bubbles; make deb32mini'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('devmin.64mini'):
        cmd ="'cd /home/pepadmin/bubbles; make dev64mini'"
        run_build = sshx + cmd
        os.system(run_build)
    elif os.path.exists('devmin.32mini'):
        cmd ="'cd /home/pepadmin/bubbles; make dev32mini'"
        run_build = sshx + cmd
        os.system(run_build)
    else:
        print("You are Not building Mini ISOS!")



runopb()
runxfce()
rungfb()
runloadedxf()
runloadedgfb()
runloadedcin()
runserver()
runmini()








